# Free major and minor scales for piano sheet music

Learning the major and minor scales for piano are one of the fundamental
skills in a musician's education. This pack includes ten pages of scales
with the following features.

 - Scales include major, harmonic minor, and melodic minor varieties.
 - Scales ascend and descend two octaves.
 - Scales include all keys, beginning at C major, followed by G major, D major, etc until F sharp major, then switches over to D flat major, and descends in flats until F major.
 - These scales are optimised for piano, with two staves, one for the left hand and one for the right. Fingering numbers are noted, but only where fingering changes occur, instead of above every single note, which is unnecessarily ugly.
 - All PDFs available in A4 and letter paper sizes

A sample page is shown below:

![Preview of major and minor scales sheet music](major-minor-scales.png)

The sheet has no copyright or attribution text that might
get in the way of professionalism when presenting to
students. The sheet is created using
[LilyPond](http://www.lilypond.org/), which is quite
possibly the world's best music engraving software.

 - [Download major-minor-scales.zip](major-minor-scales.zip)

![Free piano major and minor scales sheet
music](free_piano_major_minor_scales_sheet_music.png)
