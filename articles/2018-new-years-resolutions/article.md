# 2018 New years resolutions

The first half of January's resolution probation period has ended, and
so it's perhaps safe to post the goals for the year. So in no particular
order, here they are.

-   **Blog more**. There's a lot that's been happening, and very little
    of it sees the light of day online. There are plenty of projects to
    provoke, reflect upon, or just answer your organic search query. My
    blogging habits used to be a couple times a week, and slowly died
    down as life took over. It certainly shows in the analytics
    dashboard. By the end of the year, monthly sessions should equal the
    same numbers seen in 2015. This means content creation, content
    creation, and more content creation. You can probably already see
    that a mobile friendly theme has been refreshed, new categories, and
    a few posts already published.
-   **Divest**. Financially, investors in their 20s can take a long-term
    view. This is the time to build up investing habits, and experience
    different markets. By the end of the year, I would like to invest in
    20 different markets and start understanding my risk profile. Last
    year I experienced managed funds, blue-chip stocks, and rode the
    crypto currency roller coaster. This year will be more.
-   **Consume intelligently**. The environment is changing. Now is as
    good a time as any to build habits to be a more ethical consumer. We
    vote with our dollars, and it is our responsibility to support
    supply chains that promote good values in our society. Once
    consumed, we should break the disposable habit that arose sometime
    in the previous generation, and go towards zero-waste.
-   **Improve digital security**. The crypto boom is the public's first
    taste of moving more traditional assets into a decentralised
    network. Unlike centralised systems, decentralised systems are
    *very* hard to kill. I foresee more of our digital lives being
    interconnected, even if we don't realise it. It is pertinent that we
    promote more usage of privacy practices, such as password managers,
    secure protocols, self-hosted infrastructure, encryption, and
    signing.
-   **Begin longer term work and life**. I've been in the architecture
    industry for a year and a half now after being primarily in
    software. It's probably time for training wheels off, and to start
    specialising in an area of architecture that is socially beneficial.
    Similarly, despite the prohibitive housing costs here in Sydney, the
    ongoing market correction suggests it's time to revisit settling
    down in the more traditional sense.

Until 2019, then.
