# Free Buddhist vector logos

A few years ago I created some custom Buddhist vector logos for some CD covers.
The CDs would hold talks by Bikkhu Bodhi, an American monk who is famous in the
Theravada Buddhist traditions in authoring and teaching. Here's an example of a
CD cover:

![A CD cover for Bikkhu Bodhi](cdface.svg)

These vector logos were created using the free and open-source software
[Inkscape](https://inkscape.org/), and are available under a CC-BY-SA 4.0
license. Using Inkscape, you can open the logos, change their colours, and
resize them and change them for any format, such as PNG.

Here is the first logo below. It's the typical prayer symbol of most religions,
combined with the silhouette of a Stupa. A Stupa is quite a prominent Buddhist
architectural style that basically came from the design of a ancient tombstone.
If you think about it, it's quite creepy.

 - [Download logo 1 SVG](logo-1.svg)

![Logo showing praying hands forming the silhouette of a stupa](logo-1.svg)

The second logo sports one of the typical hand positions for meditation, and the
Bodhi leaf comes from the Bodhi tree, a significant tree for Buddhists. The
Bodhi tree has the botanical name _Ficus religiosa_, and this means it's
essentially a Fig tree. Like most Fig trees, it has formed a symbiotic
relationship with a single species of wasp, and this particular wasp species
helps pollinate the tree exclusively. Unlike most fig trees, it doesn't have the
telltale V-shaped vein at the base of each leaf. Odd.

 - [Download logo 2 SVG](logo-2.svg)

![Logo showing meditation hands forming the silhouette of a Bodhi leaf](logo-2.svg)

The third logo if a typical person meditating. Nothing fancy here.

 - [Download logo 3 SVG](logo-3.svg)

![Logo showing person meditating](logo-3.svg)

If you like these logos, you may also be interested in [Practical
Abhidhamma](../practical-abhidhamma-course-theravada-buddhists/article.md),
a free online introductory book about the Abhidhamma, a Buddhist concept.
