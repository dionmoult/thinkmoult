# Blender 3D printed Suzanne model

Here's a 3D printed monkey head using Blender.

![Suzanne, the monkey mascot from Blender](IMG_4116.jpg)

It's [Suzanne ](http://en.wikipedia.org/wiki/Blender_(software)#Suzanne)! It's
Blender's mascot monkey 3D printed with a [Makerbot](http://www.makerbot.com/).
It's about 45x40x50mm from a 3mm black plastic spool, and sits on your desk on
on your keyboard staring at you with its docile eyes.

It's a little lumpy, mainly due to the low resolution of the printer, but a
little bit more planning could've improved it a little - as seen below:

![A lumpy 3D print](IMG_4117.jpg)

You can see where the scaffolding was attached as well as where plastic
dripped down, causing some unevenness in the layering.

For the technically-inclined, the model was exported to `obj`, then re-exported
to `stl` in Rhino (because Blender's `stl` didn't seem to work), and then the
rest is standard as [documented here](http://wiki.makerbot.com/how-to-print).
