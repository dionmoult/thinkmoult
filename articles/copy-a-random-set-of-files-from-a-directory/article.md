# Copy a random set of files from a directory

More for archival purposes than anything, today I wanted to copy some
songs out of my serious mess of a music "collection" onto my microSD
card. I didn't want to have to choose and I haven't rated my songs so
that wouldn't help. Instead I wanted a random selection of songs. I'm
not an expert at `bash` but this is what I ended up using - after symlinking
all of the various directories I had my files under together:

```
$ find -L /home/drive/music -type f -name "\*.mp3" | sort -R | tail -n100 | while read file; do cp "$file" /media/disk/music/; done
```

`-n100` represents how many files are going to be copied.
