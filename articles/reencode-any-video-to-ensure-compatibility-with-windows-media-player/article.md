# Reencode any video to ensure compatibility with Windows Media Player

Other very useful tip I picked up when doing video manipulation the
other day that deserves its own post is reencoding any video so that it
will work on a vanilla Windows Media Player (without any other codecs
added). Windows Media Player is usually distributed with a very limited set of
codecs, and unfortunately if you produce a video for the general public to
view, you need to make sure WMP is happy to play it.

The tool for such a job is obviously `ffmpeg`, but the suggested
commands on the [compatibility page](http://ffmpeg.org/compat.html) of
their site seem to compress the videos to a horrendous state at the same
time, so after asking on their IRC channel on freenode this is the
command that turned up:

```
$ ffmpeg -i input -acodec libmp3lame -ab 128k -vcodec msmpeg4v2 -qscale 3 output.avi
```

Wonderful. Now I can render to whatever I please and worry about
compatibility later.
