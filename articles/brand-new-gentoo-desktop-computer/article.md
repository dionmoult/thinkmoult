# Brand new Gentoo desktop computer

It's 2018, and my 5 year old trusty [Thinkpad
420i](../another-year-awaits/article.md) has decided to
overheat for its last time. After more than 10 years of laptops, I
decided to go for a desktop. I spoke to a fellow at [Linux
Now](https://www.linuxnow.com.au/), who supplies custom boxes with Linux
preinstalled, and are located in Melbourne, Australia (as of writing, no
complaints with their service at all). A week later, I was booting up
and my old laptop was headed to the nearest e-waste recycling centre.
Here's the obligatory Larry cowsay:

    $ cowsay `uname -a`
     _______________________________________ 
    / Linux dooby 4.12.12-gentoo #1 SMP Tue \
    | Nov 28 09:55:21 AEDT 2017 x86_64 AMD  |
    | Ryzen 5 1600X Six-Core Processor      |
    \ AuthenticAMD GNU/Linux                /
     --------------------------------------- 
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||

Being a desktop machine, it lacks portability but this is mitigated as
you can [run Gentoo on your
phone](../installing-gentoo-android-chroot/article.md). Combine
your phone with a Bluetooth keyboard and mouse, and you have a full-on
portable workstation. Your desktop will be much more powerful than your
laptop, at half the price.

![Tower machine](20180105_150558.jpg)

And of course, here are the hardware specs.

-   AMD Ryzen 5 1600X CPU ([5 times
    faster](https://www.cpubenchmark.net/compare.php?cmp%5B%5D=757&cmp%5B%5D=3000)
    than my laptop) As of writing, these Ryzens are experiencing some
    instability related to [kernel bug
    196683](https://bugzilla.kernel.org/show_bug.cgi?id=196683), but the
    workarounds in the bug report seem to solve it.
-   NVIDIA GeForce GTX 1050Ti GPU ([16 times
    faster](https://www.videocardbenchmark.net/compare.php?cmp%5B%5D=117&cmp%5B%5D=3595)
    than my laptop). Yes, proprietary blob drivers are in use.
-   16GB DDR4 2400Mhz RAM
-   250GB SSD
-   23.6in 1920x1080 16:9 LCD Monitor
-   Filco Majestouch-2, Tenkeyless keyboard. If you've never needed a
    clean-cut professional mechanical keyboard that isn't as bulky as
    the [IBM Model
    M](http://thinkmoult.com/the-one-and-only-ibm-model-m-keyboard/),
    I'd highly recommend this one.

![Filco Majestouch-2, Tenkeyless keyboard](20180105_153207.jpg)

Software-wise, it is running [Gentoo Linux](http://gentoo.org/) with
[KDE](http://kde.org/). Backup server is hosted by
[rsync.net](http://rsync.net/). The worldfile is largely the same as my
old laptop, with the addition of
[newsbeuter](https://en.wikipedia.org/wiki/Newsbeuter) for RSS feeds.
Happy 2018!
