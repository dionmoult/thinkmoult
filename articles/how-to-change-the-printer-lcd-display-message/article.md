# How to change the printer LCD display message

Most office printers have an LCD display stating some sort of "ready" message.
It's possible to change this text to a custom message. I stole this technique
from [another
blog](https://web.archive.org/web/20100327104413/http://miscellany.kovaya.com/2007/10/insert-coin.html)
that is now closed, so I have largely rewritten this to preserve the method.

![A printer that is out of cheese](cheese.jpg)

There's a command in the HP Printer Job Language (HPPJL) that allows you to
change this message, and here's a script written in Perl that does just that
(not written by me):

```
#!/usr/bin/perl

  # $Id: hpsetdisp.pl 2 2008-07-10 00:05:58Z yaakov $

  # hpsetdisp.pl  
  # Connects to a JetDirect equipped HP printer and uses 
  # HP's control language to set the ready message on the
  # LCD display.  Takes an IP address and message on the
  # command line. My favorite message is "INSERT COIN".
  # Keep in mind the limitations of the display when composing 
  # your clever verbiage.
  # 
  # THIS PROGRAM IS PROVIDED WITH NO WARRANTY OF ANY KIND EXPRESSED OR IMPLIED
  # THE AUTHOR CANNOT BE RESPONSIBLE FOR THE EFFECTS OF THIS PROGRAM
  # IF YOU ARE UNCERTAIN ABOUT THE ADVISABILITY OF USING IT, DO NOT!
  #
  # Yaakov (http://kovaya.com/)

use strict;
use warnings;

unless (@ARGV) { print "usage: $0 <ip address> \"<RDYMSG>\"\n" ; exit }
if ($ARGV[3]) { print "Did you forget the quotes around your clever message?\n" ; exit }

my $peeraddr = $ARGV[0];
my $rdymsg = $ARGV[1];
chomp $peeraddr;

use IO::Socket;
my $socket = IO::Socket::INET->new(
    PeerAddr  => $peeraddr,
    PeerPort  => "9100",
    Proto     => "tcp",
    Type      => SOCK_STREAM
    ) or die "Could not create socket: $!";

my $data = <<EOJ
\e%-12345X\@PJL JOB
\@PJL RDYMSG DISPLAY="$rdymsg"
\@PJL EOJ
\e%-12345X
EOJ
;

print $socket $data;
```

I may have run this script on most computer labs at the school just before our
exam leave. Some may be on loop reciting various lyrics. If you're reading
this, just power-cycle your printer and it'll reset back to normal.
