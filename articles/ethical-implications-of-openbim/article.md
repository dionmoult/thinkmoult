# The ethical implications of OpenBIM

OpenBIM is an international initiative to use open data standards in the
architecture, engineering, and construction industry. This is in contrast to the
current state of the AEC industry, which is largely limited by the tools and
closed ecosystems of a few software vendor monopolies.

The solution posited by OpenBIM is to switch to _open data_. You may have
already heard of them, such as the IFC format. In fact, IFC is just one of many
OpenBIM initiatives, such as BCF, bsDD, and OWL. Without deciphering the funny
acronyms, it is suffice to say they are all simply initiatives to create open
data in different contexts.

In short, open data is a digital format that everybody can read and write. This
by itself is extremely interesting from a technical point of view, but in this
article I would like to focus on the ethical implications.

## Open data prevents monopolies

The biggest advantage of open-data is that it allows interoperability between
different software. This has the obvious consequence of preventing industry
monopolies. As you'll find in any standard business textbook, monopolies stifle
innovation and give rise to customer exploitation. This is no surprise to
anybody in the industry. As one example, I describe [multiple sustainability and
light simulation tools that have been killed by
Autodesk](../a-comparison-between-radiance-and-other-rendering-engines/article.md).
I have also documented the [daily problems with Revit that have been unfixed for
a decade](../why-revit-is-shit/article.md).

In the current monopolistic state of affairs, the quality of the data that
describes our built environment is not governed by the government, instead, it's
governed by Autodesk. It is time we asked ourselves the question: _is leaving
Autodesk in charge of international building data really in the best interest of
society?_ Who should dictate the digital capabilities that help design the
environment around us?

Shouldn't it be _society_?

## Open data allows design validation, to learn what makes good environments

The average lifespan of a building built today is 50 years if we're feeling
optimistic. The average lifespan of building data is 3 years. This is because
the supported software version of Autodesk products is 3 years. Admittedly,
companies can upgrade from one software version to the next, but any developer
with a large real estate portfolio will find it uneconomical to do this. Also,
anyone hired to endlessly upgrade Revit projects will kill themself after the
first year.

This usually isn't a problem because the building data quality is so poor to
begin with due to inefficient and ineffective authoring tools. It is often
cheaper to simply resurvey a site, remodel a building, and drill a few
investigative holes here and there to see what's inside.

The good news is that as authoring tools inevitably improve, without vendor
lock-in, a BIM model will last practically forever, especially because OpenBIM
standards are based on ASCII syntax. As a case in point, I can open an [IGES
file](https://en.wikipedia.org/wiki/IGES) from 40 years ago. This means that the
data can be used further throughout the building's physical lifespan.

This increased lifespan of the building has a significant ethical impact,
because it allows the building to be validated against its initial design
intent. This is to do with the concept of architectural liability. Whilst
building system engineers may have performance based contracts, architects are
simply not liable for the space that they design. When things don't work out and
a space is unsuccessful, it is hard to pinpoint what went wrong, and how to
improve it next time.

This means that the time taken for an architect to learn if his design is good
or bad from concept phase to post-occupancy evaluation is a feedback loop of
around 10 years. This makes it incredibly hard for architects to learn how to
actually design better spaces for society. If the physical performance of all
buildings are linked back to building element data, it will give people an
opportunity to study deeper what makes good spaces for society, rather than
making a very expensive gambling mistake with millions of dollars of investment,
and trying better the next time.

## Open data stores higher quality data on things that matter

It is well known that OpenBIM data improves the data quality by specifying
standardised data structures. However, these are technical advantages, not
ethical advantages. Where there is an ethical advantage is the amalgum and
variety of interrelated data that OpenBIM can capture.

For example, most BIM users nowadays are doing little more than traditional 3D
modeling that has been done for many decades, with a bit of metadata sprinkled
on top. In fact, programs like Revit expose BIM data literally in the form of
key-value metadata pairs, which pretends that a building is a 3D shape with some
attributes.

This couldn't be further than the truth. Buildings are about relationships
between objects and a merging of many disciplines which are politically
controlled by many stakeholders. In OpenBIM, you can store everything from the
prevailing wind direction, to exactly who is responsible for a building element,
to the eutrophication per unit of specified equipment, all in the one BIM model.
This variety of data that is combined in a structured way guides designers on
what data is important to different parties.

This is vital because it allows people to capture the holistic network of
relationships that a building truly is. Without this understanding, the barrier
to analysing a side effect of a design change is a one week turnaround time and
a dead tree report that gets forgotten. This delay and isolated design process
inherently creates bad design, which is in turn bad for the environment.

Another impact on data quality is due to a side effect of better
interoperability. Without vendor lock-in, specialised software can be used. This
is important because the number of features a software has is usually inversely
correlated with how good that software is at executing any particular feature.
It would make much more sense if you could use a dedicated 3D modeling tool for
3D modeling, use a dedicated data management tool for BIM data, and then linked
the two together using open data. This would also improve the quality of
designs, because they could record the designers intent with much more fidelity
than lower fidelity one-size-fits-all tool compromises.

## Open data is a prerequisite to human rights

We don't often hear the wild claim that file formats are connected to human
rights.  However, in the digital age and most of our lives invested in the
digital sphere, our digital rights are synonymous with our human rights.

The digital world we live in has changed drastically since the birth of personal
computers, and perhaps not quite for the better in some ways. I will invite you
to read more about [the five fundamental facets of ethical
software](../five-fundamental-facets-of-ethical-software/article.md), where I go
into more detail. I describe some of the worrying trends in the virtual world,
and how open data is one of the prerequisites to a solution.

## Open data is a public good

Any building, even if it is designed, built, owned, and run by a private
company, has public positive and negative externalities. Just as it is
impossible to isolate any part of the earth from its interdependency on any
other part of the earth, our buildings are no different. Yet in our daily
practice, we so easily refer to "man-made" objects versus "natural" objects, and
draw a thick red boundary line around our site bounary.

Some governments, such as the Australian government, have pushed for open-data
standards on geoscience and GIS datasets. They do this because they realise that
information about the city is a public good. If it is made available to
everyone, everyone benefits - they can scrutinise the public realm, easily
create new maps, census analysis, and proposals to change things.

Yet for some reason, public open data about the buildings which make up most of
the built environment are not available. There is no public repository of BIM
data with downloadable buildings. We can't find publicly accessible data of
building resource usage. As a society, shouldn't we be able to? The open-data
logic to benefit the public stops at the building scale. After all, this is the
most human scale of our environment, and so it matters the most.

Designing a building in an open-source fashion also fundamentally _changes who
you are as a designer_. As seen in the software industry, when you create
software that is open-source, there is a level of care bestowed upon it because
you are presenting it as a gift for the public, not as a product to be sold.
This is reflected in the quality of the open-source thing that you create,
because a part of your knows that you aren't just chasing a design and a profit
target, you're serving the public. I recall a firm called
[OpeningDesign](http://openingdesign.com/), which designs in the public domain.
Without open data, it is impossible to achieve this.

I wait for a day when it is legally required for all developments to be done
transparently. For all BIM data to be open and available to all to investigate,
criticise, and propose new ideas.

Maybe we need more transparency.
