# How to view BIM IFC files on Linux

Architectural, engineering, and construction BIM models should be written into
the international ISO standard IFC format. This allows the BIM models to have a
consistent dataset that can be queried and integrated with other BIM tools and
databases, regardless of the technology vendor. One example is where you can
[open an IFC file in Microsoft
Excel](https://www.nist.gov/services-resources/software/ifc-file-analyzer) and
use this Excel spreadsheet to perform BIM queries.

![An IFC file integrated into Excel, viewed with open-source LibreOffice
Calc. Here it shows IfcMaterial layers.](ifc-viewer-excel-libreoffice.png)

Other than Windows, it is important to consider platforms that are less widely
used in the architecture industry, such as Linux. Linux is especially conducive
towards open-source development, rapid protoyping, and is the gold standard for
data processing. If somebody is using a IFC viewer that is not open-source and
not cross-platform, that IFC viewer cannot be considered to be an authoritative
implementation of IFC, as it does not allow inspection of the code, and doesn't
prove cross-platform compatibility which is the purpose of IFC. For instance,
the popular [Solibri Model Viewer](https://www.solibri.com/solibri-model-viewer)
_cannot_ be considered an authoritative model viewer.

The tools I will introduce below are all open-source and cross-platform, and so
despite the misleading title of this article, you can use these tools to view
IFC files freely and with a battle-tested implementation on Windows, or even
Mac. However, like most open-source things, things are simply easier to tinker
with on Linux.

If you aren't already aware, it is easy to open an IFC file directly in a text
editor such as `Vim`, and read the IFC source manually. So theoretically you can
parse IFC data on your toaster. However I am assuming that you are looking for a
graphical output, as most BIM models include 3D geometry. To get these following
tools running, you may be required to compile software, and / or write
some scripts. These tools are expected to mature in the future.

You may download some simple test IFC "units" from the [IfcOpenShell test
directory](https://svn.code.sf.net/p/ifcopenshell/svn/trunk/test/input/), or
download the programmatically generated
[IfcOpenHouse](http://blog.ifcopenshell.org/2012/11/say-hi-to-ifcopenhouse.html)
for a more complex model, or if you want something a bit more detailed (although
it does contain Revit exported property sets), you can look at the [Duplex
house](https://www.nibs.org/page/bsa_commonbimfiles#project1).

## View and edit IFCs with FreeCAD

[FreeCAD](https://www.freecadweb.org/) is probably the simplest approach towards
viewing IFC files on Linux.  FreeCAD is an open source general purpose
parametric CAD modeler. Originally designed for mechanical engineering, it
supports many engineering tools such as finite element analysis (FEA),
computational fluid dynamics (CFD), and reads and writes STEP and IGES, the
grandfathers of IFC. Now, FreeCAD supports BIM modeling and can be largely
considered to be the only open-source equivalent of the current proprietary
industry tools like Revit or ArchiCAD.

Unlike Revit or ArchiCAD, IFCs are treated as first-class citizens in FreeCAD,
and so where [Revit fundamentally doesn't know what a Building
is](https://thinkmoult.com/why-revit-is-shit.html), FreeCAD has much better
support of IFC, and has demonstrated round-trip parametric IFC modeling. This
feat is something that some Revit aficionados may claim is impossible, but they
are wrong.

As such, opening and editing an IFC in FreeCAD literally as simple as going
"File->Open".

![FreeCAD opening and editing an IFC file natively](freecad-ifc-viewer.png)

At the time of writing, IFC support is largely a bleeding edge thing, and so I
would recommend getting the latest [unstable AppImage
release](https://github.com/FreeCAD/FreeCAD/releases) with support for Python 3,
or [compiling FreeCAD](https://www.freecadweb.org/wiki/CompileOnUnix) yourself.

FreeCAD IFC support depends on [IfcOpenShell](http://www.ifcopenshell.org/).
IfcOpenShell, among other things, provides a Python module which can parse and
interrogate IFC files. I recommend building it for Python 3, and match the
Python version used in FreeCAD.

If you are experimenting with your IfcOpenShell-python module, you can ensure
FreeCAD gets the right module by changing the Python path in the FreeCAD Python
console:

```
>>> sys.path.append('/path/to/site-packages/ifcopenshell-python')
```

However, note that FreeCAD is not a dedicated IFC viewer, and so like all
generalist programs, it may not have the most compliant IFC implementation. It
may also not be the fastest import.

## View IFC geometry and properties with Blender and IfcOpenShell

Another simple approach is to try the [IfcOpenShell Blender
plugin](http://www.ifcopenshell.org/ifcblender.html). This'll import the IFC
into Blender and preserve IFC properties as Blender object properties. Blender
object properties are basically key-value pairs, whereas IFC properties, like
all STEP attributes, are essentially a graph. This means that although it may be
a quick and easy way to visualise the geometry and see basic element attributes,
it may not accurately translate the complex relationships that IFC actually
supports. In the example below, you can see objects are named after `IfcName`,
unless it is blank, in which case it is named after the GUID.

![Blender IFC import showing basic IFC properties](blender-ifc-import.png)

This method is great if your purpose is mainly visualisation and rapid
prototyping. Blender works very well with VR, 3D printing, and also supports
tools for sun path simulation and CFD. For instance here is a derived `IfcSpace`
that has been used to help create a wind simulation. In the example below, I was
testing wind funnelling into the entrance of a shopping centre.

![CFD analysis in Blender using IFC as a base, here shown in OpenFOAM](blender-ifc-cfd.png)

When you are testing these massing models in Blender, you can always round-trip
it back into IFC using FreeCAD.

## View IFC geometry and 2D plans with IfcConvert

If you are only after geometry, there is a simpler approach. There is
[IfcConvert](http://www.ifcopenshell.org/ifcconvert.html), again from
IfcOpenShell. With this you get geometry and `IfcName` in open formats like
`.obj`, `.dae`, `.stp`, `.igs`, `.xml`, and `.svg`. For 3D geometry formats like
`.obj`, vertex welding is supported. However, you may notice a lack of NURB, or
surface tesselation (subdivision) support, which is available in IFC4, but not
yet so easily translatable.

The final 2 formats are interesting as they offer the ability to see the
hierarchy of IFC for other tools that might want to parse XML (but not parse
IfcXML, which is more complex), and to see 2D plans in `.svg` format, which is
the world standard vector format (in short, it's like `.dxf` but better).

Once you have it in an open format you can view it using a 3D program like
[Blender](https://www.blender.org/) or a 2D program like
[Inkscape](https://inkscape.org/).

## View IFC structure and geometry with IfcPlusPlus

You may be aware that on Windows, there is a free IFC viewer that is pretty
decent called [IfcQuery](http://ifcquery.com/). This is a proprietary front-end
to the open-source [IfcPlusPlus](https://github.com/ifcquery/ifcplusplus)
backend. As the name suggests, this implementation is C++ based, instead of
Python. I guess that means it has the potential for more optimisation, but we'll
see.

Despite the Windows interface being closed, a simple viewer is distributed with
IfcPlusPlus. It is very easy to compile, but first you need to ensure the
following packages (or equivalent on your disto) is available:

```
dev-games/openscenegraph
x11-apps/xrandr
media-libs/tiff
app-text/poppler
gnome-base/librsvg
x11-libs/cairo
net-misc/curl
x11-libs/gtkglext
sci-libs/gdal
media-libs/libsdl
media-libs/gstreamer
media-libs/openjpeg
```

You will then compile it just as you would any other C++ program:

```
$ cmake . && make && make install
$ SimpleViewerExample
```

Here you can see it running in action:

![SimpleViewerExample based on IfcPlusPlus running on
Linux](ifcplusplus-ifc-linux-viewer.png)

## View IFC geometry and attributes with IfcOpenShell Viewer

Finally, and with the most customisation, you can run the IfcOpenShell Python
Viewer. This viewer is seemingly not mentioned anywhere but [only in
passing](http://www.pythonocc.org/category/quick-examples/) and through
inspection of the IfcOpenShell codebase.

If you did inspect the IfcOpenShell codebase you might come across this
[qtviewer](https://github.com/IfcOpenShell/IfcOpenShell/tree/master/src/qtviewer),
which you can probably get to build but it is meant to work with Qt4. However,
it is pretty sparse in terms of features. But worry not as it is a red herring
and there is actually a much more powerful viewer hidden in the code.

You will need a few dependencies to compile IfcOpenShell's viewer, including:

```
sci-libs/oce
sci-libs/opencascade
media-libs/opencollada
```

With these dependencies you can compile [PythonOCC](http://www.pythonocc.org/),
which provides a Python based interface completely built upon OpenCascade, which
does all of the geometry and STEP heavy lifting. With PythonOCC, as long as you
have the IfcOpenShell python module, you can create a quick `app.py` and run
this code in it.

```
#!/bin/python
from ifcopenshell.geom.app import application
application().start()
```

This'll launch a viewer from [this IfcOpenShell
code](https://github.com/IfcOpenShell/IfcOpenShell/blob/master/src/ifcopenshell-python/ifcopenshell/geom/app.py#L179)
which will have a tree viewer, attributes, and all the rest of it. As you can
see in the screenshot below it also parses property sets very well.

![IfcOpenShell Python application running on
Linux](ifcopenshell-ifc-linux-viewer.png)

IfcOpenShell's Python module also has some pretty powerful ways to slice and
dice IFCs, some of which is mentioned in the [IfcOpenShell
blog](http://blog.ifcopenshell.org/) and some in the [IfcOpenShell
Academy](http://academy.ifcopenshell.org/). From these examples you can write
code snippets that run in real time to perform operations on the IFC file, like
generating new geometry, cutting cross sections, and calculating statistics.
