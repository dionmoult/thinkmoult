# Practical Abhidhamma Course for Theravāda Buddhists

Today, I'd like to briefly introduce a project for Theravāda Buddhists.
Buddhism, like most religions, have a few sacred texts to describe their
teachings. One of these texts, the "Abhidhamma", is rather elusive and
complicated to understand. My dad has been teaching this difficult topic
for the past 15 years, and over the past year and half, has written a
200-page introductory book for those who want to see what all the fuss
is about. It's chock-full of diagrams, references, and bad jokes.

To quote from the actual page:

> There are eight lessons in this course covering selected topics from
> the Abhidhamma that are most practical and relevant to daily life.
> Though it is called a “Practical Abhidhamma Course,” it is also a
> practical Dhamma course using themes from the Abhidhamma. The Dhamma
> and the Abhidhamma are not meant for abstract theorizing; they are
> meant for practical application. I hope you approach this course not
> only to learn new facts, but also to consider how you can improve
> yourself spiritually.

So, click to go ahead and [learn about the
Abhidhamma](http://practicalabhidhamma.com/).

![Practical-Abhidhamma](practical-abhidhamma.png)

I had the pleasure of helping on various technical and visual aspects,
and I'm happy to launch
[PracticalAbhidhamma.com](http://practicalabhidhamma.com/) which will
serve the book as well as any future supplementary content. For those
interested, the book was typeset with LaTeX, with diagrams provided by
Inkscape with LaTeX rendering for text labels.
