# Blender artwork: something's not quite right

Note: extra comments
[here](http://blenderartists.org/forum/archive/index.php/t-349586.html)

It's not often that I show my Blender artwork nowadays, but here's three
samples that I hope you'll appreciate.

Oh look. A tree with a rock.

![Hmm](The-Indiscernible-1024x1024.png)

No wait. That's not a rock. It's a heart.

Yep, definitely a heart.

Those leaves aren't right. What is it?

But wait, there's more!

![Ahum](The-Imperceptable-1024x1024.png)

I'm pretty sure these are hands. Perhaps more than one.

Yeah, more than one.

How about something lighter?

![Bubbles](Earth-and-Territory.png)

I didn't say this post would make sense :) But such is the nature of
this type of artwork.

Yadda yadda yadda.
