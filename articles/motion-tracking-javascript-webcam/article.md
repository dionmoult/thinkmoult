# Motion tracking with Javascript, HTML5 and a webcam

Why would you use the web for motion tracking? Simple. HTML5 Canvas is
exciting. Javascript is (pretty) cool. Combined with a lazy afternoon,
we can create an ultra simple hand motion tracking and colour
recognition system.

This isn't entirely true. It doesn't track the hand, it tracks a bright
blue bottle cap I found on the floor. Even more truthful is to say that
it tracks anything bright blue. But enough chat, here's a
[demonstration](http://alpha.thinkmoult.com/playground/public/finger/).
Just click the small button with the dash in it to get started, grab
something blueish and wave it in front of your camera. It won't be as
good as we got it since we adjusted it for specific lighting conditions,
but it's enough as a proof of concept.

We've also got pretty pictures to show. Here's one of the quick n' dirty
strap we used to embed the bottle cap in.

![Hand motion tracking](php_hand_motion_tracking.png)

And here is one of it in action.

![](php_hand_motion_tracking1.png)

You can see the (bad, hackish, thrown together) code for it in [my
playground
repository](https://github.com/Moult/playground/tree/master/finger) on
GitHub.
