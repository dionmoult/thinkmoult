# vtemplate 1.0.0 released

Today I'd like to release [vtemplate
1.0.0](https://github.com/Moult/vtemplate/tree/1.0.0). I've [blogged
about vtemplate
before](http://thinkmoult.com/vtemplate-a-web-project-boilerplate-which-combines-various-industry-standards/),
but now I am relatively confident about its maturity to make an official
release. Jeff Atwood has spoken about [The Rule of Three in Reusable
Software](http://www.codinghorror.com/blog/2013/07/rule-of-three.html),
and I'm happy to say that I've used vtemplate in far more than three
sites since then. Oh, and if you are not a web developer, this post is
probably not for you.

## What is vtemplate?

The [Git repository](https://github.com/Moult/vtemplate) says it well
enough: vtemplate is a boilerplate setup for starting new \[web\]
projects which combines various industry standards. There are a few
tweaks here and there, but otherwise it's loyal to upstream. You'll
recognise many favourite technologies used in vtemplate, ranging from
Stylus to Kohana to Behat. But before I run through these technologies,
I'd like to highlight the ideals behind vtemplate:

1.  Favour industry standards over proprietary and personal hacks
2.  Promote standards that emphasise readable, maintainable, and simple
    code
3.  Don't restrict or govern your desired code architecture

Let's take a brief tour over the awesome stuff we get with vtemplate.

### HTML5Boilerplate

You really can't go wrong with HTML5Boilerplate. It's a great piece of
collaboration by very experienced frontend web coders and very well
documented. This is a great first step to writing responsive,
cross-browser, HTML5-valid code. This also brings in so many other
frontend joys like HumansTXT, Modernizer, JQuery and so on.

### Stylus

If If you're using another CSS preprocessor, this'll show you just how
clean your CSS can really be. If you're not yet using a preprocessor ...
well, you'll soon find out why it's so awesome. Admittedly Stylus isn't
as popular as the big boys like LESS, but it has a very lenient syntax
and is easy to swap out.

### Mustache

Learn why writing logic in your templates is evil! Stop writing
Model-View-Controller and start writing Model-View-Template-Controller.
Don't let the backend team hold up the frontend or vice versa.

### WYMEditor CMS

Why are your clients modifying their site with bloated CMSes and
complex, unsemantic rich text WYSIWYGs? Keep things simple, and let your
code govern the CMS, not the other way around. WYMEditor reads and
writes directly to clean segments of frontend files and understands your
CSS. Best of all, it makes it easy to review changes with version
control. Read more about the simple CMS approach
[here](http://thinkmoult.com/php-cmses-done-right-how-to-enable-clients-to-edit-content-appropriately/).

### Kohana

Modular (quite literally split into many Git submodules), PSR-0
compatible, forward thinking and high coding standards PHP web delivery
mechanism with extremely flexible routing and HMVC architecture.

### Composer

Composer is everything you wished PEAR could've been, and more.

### Swiftmailer

Most webapps need a mailer library. Swiftmailer is a great one.

### PHPSpec2

We all love TDD, right? BDD is even better because it's semantic TDD!
PHPSpec2 provides a really simple (but powerful) BDD tool complete with
clean mocking and stubbing.

### Behat and Mink

Another great tool from the same guys who brought PHPSpec2. Whereas
PHPSpec covers all your unit testing, Behat is excellent for full stack
and UI testing, with the beauty of a semantic Gherkin syntax. Mink is
just icing on the cake, giving an easy to use abstraction to emulate
browsers.

### Phing

Test. Configure. Build. Deploy. Do it again!

So if you're developing a web application with PHP, check out vtemplate
:)

## What's new in vtemplate 1.0.0?

Well, apart from being the first release, where by definition everything
is new, there have been a few more tweaks since my last post on it.

-   Phing building, deploying and quality control with all sorts of
    goodies
-   UTF8 and URL replacement bugs fixed in CMS
-   Sahi comes to Behat
-   New "Photoshopper" driver for image manipulation needs
-   More Behat feature definitions as defined in [my post about
    behat](http://thinkmoult.com/in-order-to-discuss-bdd-as-a-blogger-i-need-to-talk-about-behat/)
-   Improved humanstxt
-   Default encourages variable page titles and metas
-   moult/contact form bundled
-   Kohana bumped to develop version
-   Simplified installation / configuration

Feel free to fork :)
