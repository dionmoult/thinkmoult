# In order to discuss BDD, as a blogger, I need to talk about Behat

If you're developing a web application, especially one that uses PHP,
you *should* know about [Behat](http://behat.org/).

Behat introduces itself as "a php framework for testing your business
expectations". And it does exactly that. You write down your business
expectations of the application, and it automatically tests whether or
not your application achieves them.

You begin every feature description with a three liner following the
form:

```
Feature: Foo bar In order to ... (achieve what goal?)
As a ... (what target audience?)
I need to ... (use what feature?)
```

This is then split up into individual scenarios of using this feature,
all of which are described using natural English following the [Gherkin
syntax](http://docs.behat.org/guides/1.gherkin.html). It then uses
[Mink](http://mink.behat.org/) which is a browser abstraction layer to
run these tests.

I've been enjoying Behat for quite some time now, and I've noticed
certain tests I need to write that come up again and again which aren't
included in the default Mink definitions.

The first is to check whether or not an element is visible. These days,
Javascript heavy UIs use a lot of hiding and showing, and often this is
vital to the business expectations of how the website should work. These
sorts of tests need a non-headless browser emulator, such as Sahi.
Simply prefix your test with the line `@mink:sahi`, and now we can use
the following definition:

```
/** \* @Then /^"(\[^"\]\*)" should be visible$/ */
public function shouldBeVisible($selector) {
    $element = $this->getSession()->getPage()->find('css', $selector);
    if (empty($element))
        throw new Exception('Element "'.$selector.'" not found');
    
    $display = $this->getSession()->evaluateScript(
        'jQuery(&quot;'.$selector.'&quot;).css(&quot;display&quot;)'
    );
    
    if ($display === 'none')
        throw new Exception('Element &quot;'.$selector.'&quot; is not visible');
}
```

... so you can now write ...

```
Then "div" should be visible
```

Worth highlighting is the `->evaluateScript()` function that is being
called. This means that anything you can check with JQuery can be
tested. This is pretty much everything.

Another useful query is dealing with images. Modern web apps have to
handle image uploading quite a bit, and often this comes with resizing
or cropping (for avatars, keeping to layout widths, thumbnails).
Wouldn't it be great if you could just write...

```
Given I have an image with width "500" and height "400" in "/tmp/foo.png"
Then the "img" element should display "/tmp/foo.png"
And the "img" element should be "500" by "400" pixels
```

... and of course, now you can. All this code is included in
[vtemplate](https://github.com/Moult/vtemplate) under the
[FeatureContext
file](https://github.com/Moult/vtemplate/blob/master/features/bootstrap/FeatureContext.php).

Happy testing!
