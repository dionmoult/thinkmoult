# Ratpoison: an efficient and minimalist WM

[Ratpoison](http://www.nongnu.org/ratpoison/) is a tiling window manager, with
a focus on removing the need to use your mouse under any circumstances
whatsoever. It's extremely efficient and minimalist.  For those new to tiling
window managers, this is in contrast to most window management systems where
windows "float" and overlap one another, and you have the options to maximise,
restore, and minimise windows to control their sizes. In contrast, windows do
not overlap in a tiling window manager. Instead, they tile. An easy way to
visualise this it by thinking of a table. A table can have rows and columns
split up into as weird a configuration you can think of, but there is only one
value per cell, and that value cannot overlap another cell. You can resize your
cells, switch values from one cell to another, split, remove and merge cells.
Not only that, but you can do this using naught but your keyboard. It's time to
push that rodent away from your computer and appreciate the keys you already
have to input information to your computer.  But why, you say, would I enjoy
having to design a complex table layout for a simple task? For a number of
reasons. Firstly, it's damned fast.  It's so fast you can split em, switch em,
merge em, and focus on what matters most: your work ... not moving around
windows so that you get a good look at them. Secondly, it uses up all your
screen space. No wasting space on window borders, taskbars, panels, etc. Every
single bit of your screen is showing useful information, and nothing more. This
is often referred to as "efficient use of screen real estate". Thirdly, it's a
minimal WM. This means it starts up quick, doesn't have many dependencies, and
is lightweight on your system space and resources. Take a quick peek at this
screenshot to show ratpoison in use:

![A screenshot of ratpoison in use in 2009 showing KDE graphical apps and command line apps](2009-02-09-005748_1280x800_scrot.png)

As you can see, ratpoison is basically shortcut driven. You use the keyboard to
do everything: open new applications, close applications, resize windows, move
windows ... well, pretty much everything. The problem with this is that the
default ratpoison bindings, whilst it may be familiar to those who use GNU
screen, may not be the most ergonomic.  The solution to achieving a wonderful
system is to use a combination of .ratpoisonrc settings and xmodmap settings.
Here is a nifty guide that should get you started with some usable keybindings.

## My Ratpoison setup.

I've customised by Ratpoison setup somewhat, and here's how it works. Feel free
to view these images directly for the full resolution.

![A blank ratpoison setup with polybar](34d112c93d51ad5c.png)

Ratpoison uses a "command key" to prefix any commands that are sent to it. By
default, this is set to `Ctrl-T`. This hand-stretching combination isn't the
most comfortable sequence and can get tiring after a while. So the first thing
I do is remove my `Caps Lock` key (nobody ever uses it anyway) and change it to
an imaginary key called `F13`. This way all I have to do is shift my left pinky
slightly to the left when I want to do something. This makes using Ratpoison
really fast! So create a file called `.xmodmaprc` in your home directory, and
put this in it:

```
remove lock = Caps_Lock
keycode 66 = F13
```

Next thing you want to do is make sure these key changes take effect before you
start the X server. So in your `.xinitrc` file in the ~ directory, before the
`exec ratpoison` line, add `xmodmap .xmodmaprc`.
 
Now that Caps Lock has been redefined to be `F13`, you will want to configure
Ratpoison to use it as the command key. Create a `.ratpoisonrc` file in your
home dir (yes, all these files are hidden with a .  prefix). Put this code:
`escape F13`. That's it!

I have extended my Ratpoison setup to also use Vi-keys for navigation. The idea
is that `hjkl` will navigate between window tiles, and `HJKL` will move window
tiles. I find this quite intuitive. I also use the page up and page down keys
to control the volume. I also use `polybar`, which is a popular status bar, and
bind the spacebar to open up a new `xterm`, which is by far the most common
program that I launch. Here's the setup:

```
escape F13
bind Next exec amixer -q set PCM 2- unmute
bind Prior exec amixer -q set PCM 2+ unmute
unbind k
bind j focusdown
bind h focusleft
bind k focusup
bind l focusright
bind J exchangedown
bind H exchangeleft
bind K exchangeup
bind L exchangeright
bind C-k delete
exec /usr/bin/rpws init 4 -k
exec /usr/bin/fbsetbg /home/dion/Images/Wallpaper.jpg
exec polybar moult
set winname class
set border 0
set padding 0 25 0 0
set bgcolor #333333
set fgcolor #CCCCCC
set barborder 0
bind space exec xterm
```

You might notice that `rpws` and `fbsetbg` is called. `rpws` sets up virtual
desktops. So `Alt-{F1,F2,F3,F4}` will switch between the 4 virtual desktops.
`fbsetbg` sets the desktop wallpaper to an image I have created. `set winname
class` makes the window names something intelligent than the default. The
border and paddings simply reduce the space between applications so I get 100%
screen real estate used. There are lots of documentation available on what else
to put in your .ratpoisonrc to configure it more.
  
Another trick is to turn my Windows Key (`Hyper_L`) into a special key, so that
when combined with another key, it launches one of my favourite programs, or
even controls my music player! Here's an example:

```
definekey top Hyper_L thisIsNotAWindowsKey
definekey top H-f exec firefox-bin
definekey top H-o exec ooffice
definekey top H-b exec blender
definekey top H-p exec mpc toggle
definekey top H-bracketleft exec mpc next
definekey top H-bracketright exec mpc prev
```

![Ratpoison running more console applications](db79be3deb684b12.png)

Don't forget if you want to try out the commands real time, use `F13`
(or whatever modifier) + `:` then type your command that you would use in your
`.ratpoisonrc`. If you want to run a shell command or app, just do `F13` + `!`
then type it in. 

A final trick is to use the `tmpwm` command inside Ratpoison to temporarily
switch to another window manager in the few cases where a tiling window manager
isn't quite the best thing to use. Alternatively, you can also use Xephyr.

![Running Xephyr to run an X server inside your X server. In this case running Fluxbox inside Ratpoison](99bae9a991ea138c.png)

Well. Good luck with Ratpoison, and I hope you enjoy using it. I know I have!
