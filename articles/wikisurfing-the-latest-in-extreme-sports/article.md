# Wikisurfing, the latest in extreme sports.

Wikisurfing is the act of a group of people starting on a predetermined
page on Wikipedia.org (in whatever language). Their objective is to
navigate through Wikipedia to another predetermined article, using only
inline links on the page excluding "See Alsos", disambiguations, and any
lists (eg: list of countries, list of singers, etc). The first person to
arrive on that page wins.

![It's Wikipedia!](wikipedia.jpg)

So for example we'd have 5 people all on the page on the [Sultan
Iskandar of Johor](http://en.wikipedia.org/wiki/Iskandar_of_Johor)
(Malaysia), who died a few days ago, all trying to click through links
to navigate to [Old Ephraim](http://en.wikipedia.org/wiki/Old_Ephraim) -
and depending on your level of general knowledge you'd know that Old
Ephraim was a very large grizzly bear that lived quite happily in Utah
until he got shot in the head by an unwitting shepherd. You'd have to
plan your strategy - you could go through the countries-america-utah
route or instead try the animals-bears-grizzlys technique. It's very
hard to find a page that you really can't find a link to, but I'm
willing to bet Old Ephraim is one of them (30 minutes without success!)

There are many variations of the game, such as one where every 5 minutes
you must switch computers, another where at random times you may click a
link on your neighbour's computer, or ones where you must navigate
through many topics in a sequence of your choice.

An experienced wikisurfer can tell you that there are certain topics
that are dead-ends and others which are self-sustaining spirals of
disaster - you will be stuck in that topic once you enter it. One
example of this would probably be some complex topic in hypothetical
physics. Once somewhere within the topic it's very hard to move to
another. Whereas in other topics, such as sociology, would be able to
link directly to the most random of articles with almost guilty
relevance.

A great way to describe this situation is that it represents the depth
of a topic. A topic where it would be hard to reach other unrelated
topics would be seen as an in-depth and technical topic, whereas one
which could easily be changed to another (such as in 5 clicks or less)
would be seen as a shallow, academically unchallenging topic. The
problem when trying to measure this is determining exactly which page
would be used as a base-page to attempt to navigate to.
