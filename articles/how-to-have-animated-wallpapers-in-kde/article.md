# How to have animated wallpapers in KDE

I've seen many people asking how to have animated wallpapers in KDE. The
current options include specialised Plasma Widgets, or the rather
limited yet specialised array of animated effects, such as desktop
globe, seasonal change, or virus simulations.

Unfortunately there isn't a native way to accomplish this, but KDE being
KDE, there's always a workaround.

The idea is to use `mplayer` to play a constantly looping, muted,
fullscreen video and tell it to play on all desktops, underneath all
apps, and not show up as a window in the taskbar, switcher, or pagers.

Here's the snippet:

```
#!/bin/bash
mplayer -fixed-vo -loop 0 -nosound -fs -name 'animbg' /path/to/yourvideo.avi
```

Save it as whatever.sh file and `chmod +x whatever.sh`. (not required but
useful for convenience)

The `-fixed-vo` flag prevents reopening a new window every type the
`-loop 0` flag is called. `-nosound` and `-loop` is self explanatory,
and `-fs` is fullscreen. The `-name` flag allows us to set a specific
window class, which will be picked up by a specific KDE window rule we
will create.

A quick note here that `mplayer` also has the `-title` flag, which we
should be able to use to create a KDE window rule for, but it seems as
though either `mplayer` creates the window and only afterwards changes the
title, or KDE has a bug, and so the KDE window rule doesn't match at
runtime.

We can then go into System settings -> Window Behaviour -> Window
Rules and press "New" to create a new window rule. Set the window class
to an exact match as shown below. For more information you can view the
[KDE Userbase page on window
rules](http://userbase.kde.org/KWin_Rules_Window_Matching/en).

![An animated background in KDE](kde_animated_backgrounds.png)

In the Size & Position tab, check Desktop, and set it to Force All Desktops. In
the Arrangement & Access tab, check Keep below, Skip taskbar, Skip pager, and
Skip switcher, and set them all to Force Yes.  Hit OK, and Apply your settings.
For more information you can again see the [KDE Userbase page on window
attributes](http://userbase.kde.org/KWin_Rules_Window_Attributes).

Tada! Now you have an animated wallpaper! You can set KDE System
Settings -> Startup & Shutdown -> Autostart to run your `.sh` file.
