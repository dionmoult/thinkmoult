# USyd Architecture Exhibition website released

Today I'd like to officially release the [Usyd Architecture Exhibition
website](http://usydarchexhibition.com/).

![USyd Graduation Exhibition Website Homepage](usydgradexhibition.png)

![USyd Graduation Exhibition Website Catalogue](usydarchexhibition.png)

Taken from the site itself:

> The University of Sydney Architecture Faculty puts together an annual
> exhibition for its graduating Bachelors and Masters students. This
> gives students an opportunity to showcase their best projects. An
> event is held to exhibit these works, and along with this a hardcopy
> curated catalogue and a digital catalogue is released.

So as expected, the site hosts this year's digital catalogue, and will
continue to host future year's submissions. There are currently about
100 submissions listed across five diverse project briefs. Feel free to
look around, but I'd like to issue a word of warning to my readers that
you might find the project descriptions more affiliated towards the
romantic and social science-esque narrative.

If you're wondering why a lot of the work is more art than design, I'd
like to highlight that we aren't incapable of making functional, logical
and real-world problem solving designs. However it does seem that a lot
of students aren't taught how to write, and end up romanticizing the
design into an artwork. That said, some designs *do* aim to be utopian
and speculative, but I guess if you're going to be spending the rest of
your life looking at glazing and bolts, you're excused for a little fun
during university.

I'd also like to get the chance to highlight my own submission on the
website.

![Flinders Street Hotel Proposal
Render](flinders-st-hotel-render-dion-moult.jpg)

My project this semester involved proposing a Flinders St Hotel. It's a
rather large scale project, and would take too long to explain fully,
even for the generous space that the online catalogue allows. I
recommend [viewing my project
page](http://usydarchexhibition.com/flinders-street-hotel) and reading
the full description there. It gives an overview of the project.

Finally, I'd like to quickly highlight the under-the-hood of the
website. The website runs on
[vtemplate](http://github.com/Moult/vtemplate/), is responsive, and has
it's technology colophon visible at its
[humans.txt](http://usydarchexhibition.com/humans.txt). In particular,
it was designed to be quite generic and highlight the work itself, and
function on a phone or iPad as you scanned QR codes during the event
itself. The entire website is open-source ([view
repository](http://github.com/Moult/usydarchexhibition/)), and I've just
tagged 1.0.0 today :)
