# Architecture IRC channel on Freenode

Most of my readers will know that despite the majority of my blog posts
being about technical content, I actually study architecture. The
crossover between these two fields from my experience seems to be rather
minimal. The computer geeks know a little about buildings, but not
enough to do much about it. Similarly, the architecture folks dabble
with computers, creating fields such as algorithmic architecture and
parametric design. This dabbling rarely turns into anything serious from
either party, and it's quite hard to find an online community of those
who are interested in both. I hope to change that with the new
architecture IRC channel on freenode.

I recall lurking with the hopes of meeting another architect in
\#architect for a while. Occasionally someone would come but never stay,
and the original channel founder left and hasn't been back for a year or
so. For this reason I have now registered \#\#architect (the double hash
prefix due to Freenode's channel rules) and will lurk yet again. I hope
by writing this blog post other architects might notice and pop in.

## What is IRC and how do I join the architecture IRC channel?

I realise that many architects might not be so familiar with what IRC
is. IRC can be thought of as an online chat room divided into channels,
which represent common topics of discussion. These channels are grouped
into networks, which are simply organisations that provide these
channels. So the full access details you need are as such:

**Network:** Freenode  
**Channel:** \#\#architect

Just like you need a program such as Skype in order to chat with others
using Skype, you will need an IRC program to chat with others on IRC. I
recommend using [downloading Quassel](http://quassel-irc.org/) - it
works on Windows, Mac and Linux.

If you don't want to use a program, you can easily chat using the
[online Freenode webchat service](http://webchat.freenode.net/). It's
super easy to get started, just type in a nickname for yourself and put
\#\#architect in the channel box, and press connect.

Finally, don't worry if nobody seems to be around, just stick around and
we'll respond when we're back at a computer.

See you in the architecture IRC channel!

**Edit: some people have popped up but leave quite quickly. Small IRC
communities are frequently inactive but need people to stick around for
it to grow. Please consider waiting a few hours, or just connecting
frequently and when somebody else is also around we'll have a chat.**
