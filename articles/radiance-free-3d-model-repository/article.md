# Why Radiance needs a free 3D model repository

Radiance, unlike other rendering engines, does not currently have a large
repository of free 3D models so that lighting simulationists can reuse assets in
their renders. For this reason, I have created the [Radiance free 3D model
repository](https://radiance.thinkmoult.com/), which will host model geometry
and model material definitions that are photometrically accurate and are
measured from real-life products. As far as I am aware, nothing like this
is available.

You can read more about how the repository is organised and what it offers in
terms of file formats, photometric accuracy, and download methods in the
repository itself. However, in this article, I'd like to describe the underlying
reason why this repository is so important.

Currently, architects who design our built environment do not have any
established workflows for accurately creating virtual prototypes of their
designs. If an architect wants to visualise their design, they commonly do so
through 2D sketches / collages, or use 3D rendering, as real-life physical
mockups are slow and expensive. However, both of these digital visualisations
are artistic impressions of the design, and often, the sketcher or the CG artist
would take quite a bit of artistic liberty. It is also unfortunately common
practice for the architect to blatantly lie in the render about what the space
will look like. In other scenarios, the visualisation would be purposely vague
or abstract, in order to not over-promise to clients about what the space will
look like, and leave it to the imagination.

There is room for creative exploration and imaginative interpretation of spaces,
but I would equally argue that there is also room for an honest, scientifically
accurate simulation to see what a space would _really_ look like. Even if this
is never shown to clients, it is an invaluable tool for architects who want to
remove the rose-tinted glasses and verify that the space is actually beautiful.

If this type of validated render is exposed to the client, and by extension, to
the public, then it serves as an honest representation of the aesthetic impact
the design has on the public domain. Currently, this doesn't exist. Instead, we
see buildings that glow from impossible bird-like angles or unlikely viewpoints,
treated as a spectacular blessed work of art upon the ground. In reality,
buildings are frankly a little bit drab to most people. Especially for projects
which heavily impact the public domain, it is ethically responsible to give them
a true representation of how designs will impact the environment.

The tool that is capable of this scientifically validated visualisation, of
course, is Radiance. Architects may come across Radiance through daylight and
sunlight studies, sustainability analysis, or through the light engineering of
the space. However, it is less often used in the form of qualitative analysis,
as it is slow, and due to the over-simplified models and material definitions,
often produces renders that look even more fake (despite being correct) than an
artists impression of a photoreal scene.

The reason for this is because lighting engineers, in their quest for accuracy,
have not had the same urgency to develop the same workflows, asset libraries,
and attention to geometric and textural detail that the artists have. These
types of things are simply not required when analysing average illuminance
levels, for instance.  However, accuracy and art are not mutually exclusive. For
instance, more detailed geometric models actually increase the accuracy of a
light simulation.  For sure, this increases the simulation time significantly,
and probably does not impact the average overall luminance and illuminance
values, but certainly does greatly impact the qualitative empathy a viewer has
with the rendered visualisation.

It is also important to stress that this type of render certainly takes a lot of
time compared to a regular artistic render, and almost always will look less
"spectacular", and time is the most important factor in creating renders. The
key to making validated renders economical is similar to how CG artists make
their renders economical: they build an asset library. The difference is that a
Radiance asset library is something quite special: the materials are true. There
will be no need to tweak objects for a particular light or shot: the asset will
simply be a digital twin of the real life object.  But unfortunately, a
repository does not yet exist, which is why this one is being launched. It is
also key to keep the repository open-source, so that the resulting assets can
then be validated by the community, and recipients can ensure they are not being
lied to about the quality of the asset.


The good news is that [rendering engines are already trending towards
photorealism](../history-rendering-engines-modern-trends/article.md). They
aren't quite there yet, and will take a long time to get there, but slowly they
are pulled towards reality. I envision a time in the future where clients will
choose between hiring two architectural firms: one of them can tell them
truthfully what the design will be like, and the other cannot. That will be the
stepping stone to the time where government and planning approval organisations
_expect_ a truthful representation in all development applications. After all,
it's the _right_ thing to do.
