# Hello SevenStrokes: Building websites ... a little differently

A few months ago, Chris Paplinski, Nathan Charrois, Kaushal Inna, Andre
Brokman, Kelsie Rose and I, Dion Moult, gathered to create a company.
Today, we would like to present to the world:
[SevenStrokes](http://sevenstrokes.net/).

[![Sevenstrokes web development](facebook_avatar.jpg)](http://sevenstrokes.net/)


SevenStrokes is a web development company but with a few key
differences.

1.  Firstly, we see websites as a service, not a product. We don't just
    build a website, we treat it as part of your larger corporate
    strategy.
2.  We build systems that mirror your day-to-day domain issues. We use a
    combination of behavior-driven development and code architecture
    that employs the same daily language that you do. This ensures our
    system makes sense not just in the software world, but in real life,
    and thus always move a step towards achieving your corporate goals.
3.  We follow many aspects of the open-source business model, ensuring
    that we assign the most motivated staff that want your site to
    succeed just as much as you do, and that full inspection guarantees
    your system integrity.
4.  We push for the latest industry standards and keep on pushing, even
    after launch. Websites are usually short-lived, but we're changing
    that with a system architecture that maximises long-term life.

So what are you waiting for? Do you need a website built? Do you need
somebody to help spearhead your latest online initiative? Check out
[SevenStrokes: Building websites ... a little differently](http://sevenstrokes.net/)

[![Sevenstrokes banner](facebook_cover.jpg)](http://sevenstrokes.net/)
