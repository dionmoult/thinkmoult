# Medical animations and surgical visualisation with MedFilm

You're sitting in front of the doctor's table in a hospital. The doctor
has just spent the past half hour explaining the procedure you will
undergo to solve a medical problem that you experience. It sounds
complicated -- there are a few things you have to do to prepare, some
foods to watch out for, and a recovery process of a few months
afterwards. You will later come home to only be bombarded with a series
of questions from your friends and family, who are all curious and have
somehow managed to ask the questions which you didn't think of asking
earlier. It also doesn't help that their native language isn't English.

Four years ago, this was exactly the problem that Erik Kylen, a small
team in Sweden, and myself working remotely set out to solve. The
solution was a series of animated videos to explain various medical
issues in simple terms. A doctor could use these videos to help guide
patients, and patients could then watch these videos from the comfort of
their own home. This is [MedFilm](https://medfilm.se/).

[![MedFilm Logo](medfilm-logo.png)](https://medfilm.se/)

Each video starts with a gentle description of the various body parts
involved in the procedure to introduce the required medical terminology.
This is followed by an explanation of how these parts relate to the
ailment at hand. The patient is then reminded of the various preparatory
steps they need to take before the procedure, such as fasting, or
drinking fluids. The surgical procedure is then shown, heavily tested to
maintain medical accuracy whilst ensuring that the patient does not see
anything gruesome. Finally, the video describes the recovery process,
and the steps the patient can take to expedite it.

These videos are simple to understand, are accessible with subtitles and
translations into many languages, and tailored for specific medical
practices in localised hospitals and countries. Each hospital and
country has their own preferred ways of doing things, and these videos
accomodate that fact.

A doctor from a participating hospital can share a link to their
patient, or interactively use the video during the briefing process on a
tablet. A patient can later watch it again to refresh their memory, or
reshare it with friends and family.

![MedFilm surgical videos on various tablets](All-Units-Cropped.png)

Let's see a demonstration video (and yes, videos can be embedded with
custom branding into a hospital or clinic's website!). Below is the
video created for an appendectomy. Usually, getting your appendix
removed is a pretty safe, standard procedure, and happens pretty soon
after you figure out you have a problem. Most people have also heard of
it, which makes it a great procedure to demonstrate.

Here's the video! Click play below and learn about an appendectomy!

<iframe src="https://medfilm.se/patientinformationsfilmer/film/appendix-removal/player" style="min-height: 400px; width: 100%; border: 0;" scrolling="no" allowfullscreen></iframe>

MedFilm is steadily growing and now has a repository of 40 videos
covering topics from cardiology to otorhinolaryngology (I'm not a
doctor, so to me that's a very complicated word!), used in clinics
across Scandinavia. I'm proud of the service, and happy that it is able
to help patients. If you're interested are are involved in the health
industry, you can [contact MedFilm here](https://medfilm.se/contact) and
we can explore opportunities!
