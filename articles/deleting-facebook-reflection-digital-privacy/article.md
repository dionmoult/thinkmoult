# Deleting Facebook, and a reflection on digital privacy

In the wake of the recent [Cambridge
Analytica](https://www.theguardian.com/news/series/cambridge-analytica-files)
privacy issue in the news, I have decided to
[\#DeleteFacebook](https://twitter.com/hashtag/deletefacebook?f=tweets&vertical=news&src=hash).
The thinkMoult blog is still represented via the public [Facebook
thinkMoult](https://www.facebook.com/thinkMoult) page, but my private
profile has been cleared out. Given that Facebook is increasingly
sharing our profile data (as shown in the graph below produced from
Facebook's very own reports), clearing out the account makes a
difference, albeit a small one. I also thought it would be good to share
a few things I've learned about Facebook in the past couple of weeks,
related to my new years resolution to [improve digital
security](../2018-new-years-resolutions/article.md).

![Facebook government requests over
time](facebook-government-requests-over-time.png)

(Note: you can compare with [Google's data disclosure over
time](https://transparencyreport.google.com/user-data/overview?metric=users_accounts))

First, I'd like to commend Facebook's behaviour so far. Being the
world's largest social network probably isn't easy, and Facebook has
made initiatives to increase its transparency. For instance, they issue
a [transparency report](https://transparency.facebook.com/), and they
use the [Signal secure messaging protocol](https://signal.org/) for a
secure chat mode in FB Messenger. It is also possible to [download your
Facebook data](https://www.facebook.com/help/131112897028467/), and
[place restrictions on data
sharing](https://www.facebook.com/help/247395082112892) with apps and
advertisers. Their [data retention
policy](https://www.facebook.com/full_data_use_policy) also seems to
suggest that if you delete data from your account, it's also gone from
their servers.

However, of course, this isn't the complete picture. Take for instance
the world map of Facebook government requests in the first half of 2017
from their very own transparency report.

![Facebook government requests in
2017](facebook-government-requests.png)

The map (split into [Jenks natural
breaks](https://en.wikipedia.org/wiki/Jenks_natural_breaks_optimization))
shows that US government requests are miles ahead of the rest of the
world in asking Facebook for information. Most governments from other
countries don't play any part in this.

However, the map is incomplete. It is also not possible to see data
shared through indirect means. Developers can easily create apps that
integrate with Facebook. Whether you answer a survey through Facebook or
use Facebook to log into another service, they can have varying degrees
of access to your profile and friend information. This may also occur
without your explicit consent. For instance, my meager Facebook usage
has resulted in my details being shared with 138 companies. This is not
to mention that Facebook trackers are on 25% of websites online. Oh, and
let's just forget Facebook altogether: Google trackers are on 75% of
websites online (and yes, also on my blog). Basically, [you are always
tracked online](https://donttrack.us/), from the [way you move your
mouse](https://clickclickclick.click/) to [how you
feel](https://en.wikipedia.org/wiki/Sentiment_analysis), which can be
combined through machine learning to indirectly define character
profiles, interests, and demographics.

Like most technologies, this data can be used for very positive things
and very negative things alike. The negative side comes when services we
assume are private social platforms are actually not. This data may be
used to [influence political
elections](https://www.theguardian.com/uk-news/2018/mar/19/cambridge-analytica-execs-boast-dirty-tricks-honey-traps-elections),
or [help China rank all
citizens](https://www.independent.co.uk/news/world/asia/china-surveillance-big-data-score-censorship-a7375221.html),
or [rebrand political news as fake news in
Malaysia](https://www.eff.org/deeplinks/2018/03/malaysia-set-censor-political-speech-fake-news),
or even be [accessed by any law enforcement agency around the
world](https://www.eff.org/deeplinks/2018/03/responsibility-deflected-cloud-act-passes)
without notification or warrant - it doesn't matter - people
misunderstand that posting on Facebook is not a private matter: it is
public.

Deleting Facebook is one step of many to promote the idea that just as
there are public outlets for expression online (blogs, Twitter,
Facebook) there equally are private outlets (Signal, Tor, ProtonMail).
Of course, there is nothing inherently wrong with either outlet, but we
should recognise these differences in privacy and know when to choose
between them.

For more reading, see [why digital rights matters, even though you don't
think it impacts
you](../digital-privacy-important-you-think-doesnt-impact-you/article.md),
and how you can [improve human rights by changing your messaging
app](../improving-human-rights-through-secure-messaging/article.md).
