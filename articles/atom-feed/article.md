# thinkMoult Atom / RSS feed

If you would like to keep up to date with latest articles and developments
published on thinkMoult, you can do so using [this Atom
feed link](https://thinkmoult.com/feed.atom). Alternatively, you can copy and
paste the link below:

`https://thinkmoult.com/feed.atom`

## What is an Atom feed?

An Atom feed is a special online link that you can insert into a program called
a "Feed reader". A feed reader will check for updates on thinkMoult, and
whenever there is a new article, it will show up in your feed reader program.
This is less annoying that email subscriptions, does not harass you with
advertising, and faster to browse than using a web browser.

Imagine a completely personalised newspaper, populated by the feeds that you
find interesting. Almost all websites that publish content online offer a "feed
link", just like thinkMoult. This includes news websites, social networks, and
blogs.

If you have heard of RSS feeds, Atom works just like RSS does, and most feed
readers can read both Atom and RSS feeds.

There are many feed reader programs available for computers and phones. If you
are on Android, I can recommend
[Feeder](https://f-droid.org/en/packages/com.nononsenseapps.feeder/), and if you
are on Linux, I can recommend [newsbeuter](https://www.newsbeuter.org/).

## Alternative social channels

I do microblogging on the fediverse. You can check it out at
[\@thinkMoult@mastodon.social](https://mastodon.social/@thinkMoult).

If you are not interested in the fediverse, you can still keep up to date with these developments by following this [Mastodon Atom feed](https://mastodon.social/users/thinkMoult.atom).

The fediverse is a distributed social network - where anybody can host their own
social network and moderate its content how they'd like, and then subscribe and
post across other networks too.  This is similar to email, anybody can host
their own email, and that doesn't stop you from emailing other people who use
different hosts. This type of social network helps prevents censorship,
discourages data mining and privacy violations, and promotes free software.

I do not post articles on social networks such as Facebook or Twitter, due to
their policies on data mining and personal privacy.
