# Use Klipper to automatically post to a Pastebin

[Pastebins](http://en.wikipedia.org/wiki/Pastebin) are a really useful
way to share snippets of text. However it's sometimes a bit cumbersome
to have to open a browser window, type in the URL, paste it in, click
submit, then copy the URL to share with your friend. That's why things
like [wgetpaste](http://wgetpaste.zlin.dk/) exist - small command-line
utilities to automate this process and return the URL. wgetpaste isn't
the only one, of course, but they're all rather similar. Klipper is
KDE's Clipboard manager - whenever you copy something it gets added to your
clipboard. Klipper allows you to navigate through it - so that you can paste
something you copied a while back, or set up custom things to paste, or even -
which is what I'll talk about today - set it to automatically perform an action
on the paste. The most common use is to automatically open a link in a browser
if you copy a link from somewhere. What we'll tackle is to get Klipper to
autopaste our clipboard item into a pastebin, and return the URL to us. So just
set it up as shown below:

![Klipper configuration screen](klipper.png)

And you're done! Copy something, press `Ctrl-Alt-R` to invoke the actions menu,
click "Pastebin", and now the URL of the pasted item will be in your clipboard
for you to `Ctrl-V` to your friend. Neat, eh?
