# Clean code, and how to write it

Note: the article was originally circulated on \#cleancode and \#kohana
on Freenode and is now recorded here as an archive. It seems very useful
as something to link people to on IRC when they have questions, so feel
free to share as well.

At [SevenStrokes](https://sevenstrokes.net/), we practice Clean Code.
Although code speaks louder than words, at the moment my public
repositories are heavily outdated. What isn't as outdated, however, is a
short introductory guide I wrote on Clean Code for the internal use of
SevenStrokes. Although it is a guide which focuses on the basics, it
does make some assumptions on the reader having some knowledge about
programming. You'll notice that the examples are primarily written in
PHP, but are applicable in all languages.

![Clean code
architectures](https://sevenstrokes.net/images/usecase_architecture.svg)

The article answers the question of why good code matters, what is good
code, and covers the three pillars of good code: syntax, architecture,
and workflow. It shows coding examples of how to write good code,
introduces you to the more abstract architectural jargon, and different
tools and processes out there.

Without further ado, please click to read: [SevenStrokes: Learn how to
write Good Code](https://sevenstrokes.net/learn-how-write-good-code).
