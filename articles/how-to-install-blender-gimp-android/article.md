# How to install Blender and The GIMP on Android

It is possible to install Blender and The GIMP on Android, with the full
functionality just as their desktop counterparts. They can be installed as
native applications as long as we have a few prerequisites.

Blender and The GIMP do not have mobile / Android native interfaces, and so as a
result we will need to set up a Linux-based "X" server environment on the phone
so that their GUI will display, and the resulting interface will not be touch
friendly and requires a bluetooth mouse and keyboard to be much use. I should
also note that most Android phones are not as powerful as desktops, and so
performance will be limited, but it is useful to know that it can be done.

To start with, you will need to [install Gentoo Linux on your Android
phone](../installing-gentoo-android-chroot/article.md). This is a
straightforward process, does not require root, and allows you to run Gentoo
inside a `chroot`, alongside your Android. As it's in a `chroot`, you can use Gentoo
through a terminal emulator. As we will require a desktop interface, you will
need to install `xorg-server`, a VNC server, and some sort of window manager (in
this case, I use `fluxbox`). I use Android's bVNC free app as a VNC client, as
it supports converting the screen to a touchpad, but you may choose any you
like.

```
$ emerge xorg-server tigervnc fluxbox
$ vncserver -geometry 960x540
```

You can watch this video demonstrating a working [Gentoo on Android on
Peertube](https://peertube.social/videos/watch/ad395c9b-9702-4060-ac05-4c94b64956ab),
or embedded as a video below.

<iframe src="https://peertube.social/videos/embed/ad395c9b-9702-4060-ac05-4c94b64956ab" style="min-height: 400px; width: 100%;" frameborder="0" sandbox="allow-same-origin allow-scripts" allowfullscreen="allowfullscreen"></iframe>

Finally, install Blender and The GIMP just as you would on a desktop. You will
need to unmask them as they are technically untested on an `arm` processor,
which your phone likely uses, but I can guarantee that it does compile, and
certainly works fine on my Samsung Galaxy S7.

```
$ emerge blender gimp
```

And that's it! As you have a full desktop environment, you can treat your phone
as a desktop and install desktop applications. You can watch a [video of Blender
and the GIMP running on Android](https://peertube.social/videos/watch/525aa253-5ef2-410f-b4e0-2c401064005d) on Peertube here, or see my demonstration below. I also demonstrate real-time rendering with Cycles and rendering out a scene. The scene I use is a city generated from [OpenStreetMaps](../openstreetmaps-open-source-maps-application/article.md).


<iframe src="https://peertube.social/videos/embed/525aa253-5ef2-410f-b4e0-2c401064005d" style="min-height: 400px; width: 100%;" frameborder="0" sandbox="allow-same-origin allow-scripts" allowfullscreen="allowfullscreen"></iframe>

In practice, I have found that I never tend to actually use Blender and the GIMP
on my phone, as the screen is too small and it is too slow. But as a proof of
concept I reckon it's pretty cool.
