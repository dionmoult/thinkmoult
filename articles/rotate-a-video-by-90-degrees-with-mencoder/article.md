# Rotate a video by 90 degrees with mencoder

I was recently doing some video editing work where the workflow was
something like this: film in portrait, transfer to computer, rotate
videos by 90 degrees, sequence together several videos, strip out
background noise from entire video. Filming was done with a camera,
sequencing was done by Kdenlive (I've previously only had experience
with Blender's VSE and I must say I was very happy with this new
application), and the noise-stripping was done with Audacity. I must say
I'm surprised at how fast this was all accomplished and kudos to all
those developers who created these apps.

However one thing I didn't know how to do was how to rotate the video by
90 degrees. Kdenlive can do it but it ends up being awkwardly stretched
and I couldn't figure out how to unstretch it. Luckily `mencoder`, which
comes with the `mplayer` package, has got a few tricks up its sleeve.

More for my own records than for anybody else, here's the command I
used:

```
$ mencoder -vf rotate=2 -o output.avi -oac pcm -ovc lavc input.mov
```

As my input file was a `.mov` some of the sound wasn't synchronised well
after rotating, which was easily fixed by this option `-demuxer mov`. If
you want to rotate clockwise instead of anticlockwise change `rotate=2`
to `rotate=1`.
