# LearnMusicSheets - download PDFs of music sheet exercises

Today I'd like to talk about a brand new project: LearnMusicSheets. If
you are a music teacher, or are learning music, you have no doubt
searched the internet looking for music score PDFs. Examples of music
scores are major and minor scales, arpeggios, or even some blank
manuscript. I've searched before for scores to present in my lessons,
but have so far been unable to find scores with a suitable level of
quality. Specifically, I'm looking for no copyright notices, no badly
notated scores, and comprehensive. Instead, often I find scores with
horrible jazz Sibelius music fonts, inconsistent naming, or obscene
fingerings notation. On the rare occasion that I find a sheet typeset
half-decently, it is often incomplete and doesn't contain all the
relevant exercises.

So I have taken the time to notate various music scores for learning
piano and music in general. These music scores are all typeset
beautifully using the [Lilypond](http://lilypond.org) notation software.
I haven't put any copyright notices, and have provided a variety of
paper sizes and combination of exercises. I've used these myself in my
lessons for many years and they work great! Hopefully, you'll enjoy them
as much as I have!

## Beautiful music exercises to download

Today I would like to launch [LearnMusicSheets](//learnmusicsheets.com).
Learnmusicsheets is a website where you can download music sheets for
learning music. Currently, it offers blank manuscript paper, major and
minor scales, major and minor arpeggios, and intervals and cadences. All
these scores are available as an instant PDF download. More exercises,
such as jazz scales, will come soon. If you're curious, go and [download
music sheets](//learnmusicsheets.com/) now!

![Example PDF of major and minor
arpeggios](https://learnmusicsheets.com/assets/images/major-minor-arpeggios.png)

If you have any comments or suggestions, please feel free to send me a
message. Or if you use them yourself, let me know how they are!
