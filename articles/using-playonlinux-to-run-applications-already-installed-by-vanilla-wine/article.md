# Using PlayOnLinux to run applications already installed by vanilla WINE

Using PlayOnLinux to install WINE 0.9.14 to run some games, such as StarCraft,
can fix issues such as lag. If you have already installed a Windows application
using WINE, you can use PlayOnLinux to run it instead of WINE directly, and may
experience better results. This example demonstrates how to do it for
StarCraft, but you may need to modify it for your program.

First, install PlayOnLinux, Tools -> Manage wine versions -> Install v0.9.14.
Should be straightforward. Then use these commands:

```
$ mkdir /home/username/.PlayOnLinux/wineprefix/starcraft && env WINEPREFIX=/home/username/.PlayOnLinux/wineprefix/starcraft wineboot
$ cp -r /home/username/.wine/drive\_c/Program\\ Files/StarCraft /home/username/.PlayOnLinux/wineprefix/starcraft/drive\_c/Program\\ Files/
$ touch /home/username/.PlayOnLinux/configurations/installed/StarCraft
```

Now place the following in the StarCraft file:

```
#!/bin/bash  
export WINEPREFIX="/home/username/.PlayOnLinux/wineprefix/starcraft"  
export WINEDEBUG="-all"  
cd "/home/username/.PlayOnLinux/wineprefix/starcraft/drive\_c/Program
Files/StarCraft"  
wine "StarCraft.exe" $@
```

And that's it! Run PlayOnLinux and you'll be able to run the program
from there. This guide should be able to work for other scenarios as
well so feel free to adapt it.

Oh, and as for the lag? Yep - oddly enough it *did* get completely
fixed. Regression time.
