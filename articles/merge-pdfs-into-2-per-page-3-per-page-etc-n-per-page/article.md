# Merge PDFs into 2 per page, 3 per page, etc (n per page)

This post is mostly just a reminder to myself, but it may be useful to
others. I was recently asked to convert a `.ppt`, or Powerpoint slideshow
document, into a three slides per page, horizontally centered, one on
top of another, with frames around each slide. Powerpoint itself offers
several printing options, including a 2<sup>n</sup> per page (ie. 2, 4,
8, etc) as well as handouts, which can do 3 per page but are incredibly
small and have writing lines besides the slides. Luckily, Powerpoint can
save the `.ppt` into a `.pdf`, and there are plenty of PDF manipulation
tools on Linux, and are generally much more readily available than on
Windows. Scribus is an option, but after some searching I discovered
`pdfnup`, part of the `pdfjam` package. This is the command I ended
up using:

```
pdfnup --nup 1x3 --no-landscape --frame true --batch foo.pdf bar.pdf etc.pdf
```

For a full explanation of the commands, you can try looking at its
[manpage](http://linux.die.net/man/1/pdfnup), or `pdfnup --help`, which
is much more useful.
