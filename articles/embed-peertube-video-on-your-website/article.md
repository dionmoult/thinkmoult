# Embed Peertube videos on your website

[Peertube](https://joinpeertube.org/en/) is a network of websites that allow
people to upload, share, and comment on videos. It's kind of like YouTube, but
with a big difference. Whereas YouTube polices its content, has its own
arbitrary rules about advertising, revenue, and copyright, in Peertube, you can
run your own website and host your video yourself. Using a growing native web
technology known as _ActivityPub_, your website can talk to other Peertube
websites, so you can use your account to comment on videos on other websites,
and your video will be shared on other people's websites too, should they choose
to.

This way, you can design it yourself, have the peace of mind of content uptime,
and make your own rules about how you want your video sharing to run (like
upload sizes, comment moderation, and where it is and isn't shared). This is
important for content distributors who rely on their videos. For instance,
[YouTube blocked videos from the Blender Foundation and MIT OpenCourseWare
without
reason](https://torrentfreak.com/youtubes-piracy-filter-blocks-mit-courses-blender-videos-and-more-180618/).
When events like this occur, you may consider other options.


But, you say, the whole reason I put it on YouTube is because I don't want to
pay for all the infrastructure to host my own videos! This is where Peertube is
special: it uses P2P protocols, just like torrenting, to watch videos. So anyone
who watches a video is also helping host your videos for other people. The more
popular your video is, the more people are helping host your video! So you won't
need to type of infrastructure that Google has.

## Embedding a Peertube video with an `iframe`

In the example below, I show how to embed a video from a Peertube instance. This
shows a video hosted on the website [peertube.social](https://peertube.social).
You should change the link depending on the instance that you are hosting your
videos on.

If you watch your video on its instance, its URL looks a little like this:

```
https://peertube.social/videos/watch/ad395c9b-9702-4060-ac05-4c94b64956ab
```

All you need to do to embed it is change the link so that the word `watch`
turns into `embed`. Then, you can put it in a simple HTML `iframe`, and it will
work on any website. Here's an example of the same video embedded on a website,
with some CSS styling for some flexible video scaling. Pay attention to how the
URL changes below:

```
<iframe src="https://peertube.social/videos/embed/ad395c9b-9702-4060-ac05-4c94b64956ab" style="min-height: 400px; width: 100%;" frameborder="0" sandbox="allow-same-origin allow-scripts" allowfullscreen="allowfullscreen"></iframe>
```

That's it! You can see some videos embedded on another article I've written about how to
run programs like [Blender and the GIMP on your Android
phone](../how-to-install-blender-gimp-android/article.md).
