# Rip audio from an .FLV file

How do you rip only the audio from an `.flv` file? `.flv` files, or Flash Video
files are the format used in browser-embedded videos, common on video-sharing
sites such as YouTube or Vimeo. For whatever reason if you have an `.flv` file
of your favourite music video, now you can get the music rocking solo.

```
$ mencoder input.flv -o output.mp3 -of rawaudio -oac mp3lame -lameopts cbr:br=192 -ovc copy
```
