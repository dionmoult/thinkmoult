# A Beaglebone, a Blender, a Board, and a Swarm

Hardware isn't generally my thing. When it comes to software, I like to
break and create. But in my opinion, hardware should *just work*. But
even though that's another story altogether, it did explain my
apprehension when I greeted the UPS guy one morning delivering a
[BeagleBone Black](http://beagleboard.org/).

![A BeagleBone Black](beagleboneblack.jpg)

Let's begin with the BBB. It's a computer the size of a credit card,
which isn't that impressive if you realise that your phone is a
computer. I find the best way to explain it is in terms of two other
products, the [Arduino](http://www.arduino.cc/) and the [Raspberry
Pi](http://www.raspberrypi.org/). The Arduino is a similarly sized
(comes in multiple sizes though) controller where you can upload
scripts, plug in a hardware circuit (wires and lightbulb, that sort of
thing), and have it control the circuit. Despite its power in hardware
control, it only has a small scripting interface for you to do your
programming. The Raspberry Pi is the opposite. It's a full Linux
computer (based off Debian), but does not have proper hardware controls
out of the box. The BBB provides the best of both worlds: a full Linux
system (Angstrom Linux, but of course you can flash your own), and a
ridiculous number of IO pins to control circuits. All this awesome power
at 45USD.

The next step upon receiving this wonderboard was obvious. Let's build a
swarm of robots. Along with two university friends, Lawrence Huang and
Gloria Nam, we set out planning the system.

![An overview of the parts of the physical model](world.png)

The base was to be constructed out of a 1200x1200mm wooden plywood board
and cut it into a circle with a hole in the middle. This would be the
"world" where the robot swarm would live on. This world would operate
like a [Lazy Susan](http://en.wikipedia.org/wiki/Lazy_Susan), and would
have a two depots filled with some sort of resource. One at the center,
and one at the perimeter. This gave the colony a purpose: it would need
to collect resources. Above the board was where we would put the
computer, BBB, power supply, and cables to hook up to all the bots
below.

We then had to determine the behavior and movement capabilities of the
swarm. It had to act as one, but still remain separate entities. It also
had to disperse to discover where the rotated resource depots were, and
the swarm as a whole had a set of goals and quota limitations. Five
movement types (along with the math) were worked out to allow the bots
smooth and flexible movement across the terrain.

![Movement rules](rules.png)

The overmind was next. We would use [Blender](http://blender.org/)'s
very flexible boid simulator along with custom Python scripts using
Blender's Python API to simulate the swarm behavior on the computer and
set swarm goals. At the same time, a real-time top-down view could be
generated and displayed. Due to budget reasons, we couldn't build the
entire swarm of robots, but instead settled on building just one bot in
the swarm, and having this bot track the motions of a single bot on the
computer screen, but still behave as part of the full 32-robot swarm on
the screen. Viewers could then see on the screen the full swarm
behavior, and physically see a single bots behavior in front of them.

![Swarm boid simulation in Blender](swarmscreenshot.png)

The car itself was then built. It was relatively small and was barely
enough to fit the two continuous-rotation servo motors that were
required to power its left and right treads. It had a little tank on its
top to hold resources, a depositing mechanism at its front, and dragged
along a massive conveyor belt to collect resources behind it.

![A single robotic car](car.png)

Now the fun part - calibrating the simulated swarm with the actual
physical swarm behavior, and doing all the physical PWM circuits. Many
sleepless nights later it was a success. Here we see the bot doing a
weird parking job into the depot and collecting resources, going back to
the center, and depositing it. Apologies for the lack of video.

![Robot's collecting resources](collect.png)

And there we have it. A swarm of robots. Did it curb my fear of
hardware? Not entirely.

![A front shot](frontshot.png)

For those interested in the actual system, here's a macro overview:

![Overview of the system](system.png)

A few extra fun things from the project:

-   Calibration was not easy. Actually, it was very hard. No, it was
    stupidly hard. It was ridiculously hard. Real life has so many
    uncertainties.
-   Each bot is tethered to the overmind via 8 wires (3 per tread, 2 for
    conveyor belt). Could it be made into a wireless swarm? Yes. Did we
    have the money? No.
-   Could it be modified to move in 3D XYZ space like a swarm of
    helicopters? Yes. Would I do the math for it? No.
-   The actual simulation was done on the computer via Blender + custom
    python scripts. The computer was then connected via a persistent
    master SSH connection, which was reused to send simple signals to
    the pin's embedded controller. So all in all the BBB actually didn't
    do much work. It was just a software to hardware adapter.
-   Because the computer was doing all the work, it wasn't hard to add
    network hooks. This meant we could actually control the system via
    our phones (which we did).
-   Weirdest bug? When (and only when) we connected the computer to the
    university wifi, flicking a switch 10 meters away in a completely
    separate circuit (seriously, *completely separate*) would cause the
    BBB to die. Still completely confused and will accept any
    explanation.
-   Timeframe for the project? 4 weeks along with other obligations.
-   Prior hardware and circuit experience: none. Well. Hooking up a
    lightbulb to a battery. Or something like that.
-   Casualties included at least three bot prototypes, a motor, and at
    least 50 Styrofoam rabbits
-   Why are all these diagrams on weird old paper backgrounds? Why not?
-   At the time of the project, the BBB was less than a month old. This
    meant practically no documentation, and lack of coherent support in
    their IRC channels. As expected, this was hardly a good thing.

Project success. I hope you enjoyed it too :)
