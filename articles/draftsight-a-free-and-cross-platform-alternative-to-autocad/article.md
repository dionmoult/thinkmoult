# DraftSight: a free and cross-platform alternative to AutoCAD

Whilst Linux is an excellent system for programmers, it's certainly a
little wanting for people who deal with creative graphics. There are
tools like Krita, GIMP, Inkscape, Blender and Digikam and so on to help
fill this gap, but one area which isn't talked about so often are CAD
tools. As an architecture student and a Linux user, I can safely say
that the options are disappointing. It certainly is possible to have a
complete graphics workflow on Linux, but it's not as good as on Windows.

There were always CAD packages around such as FreeCAD and QCAD (I
believe rebranded to LibreCAD) and its various derivatives, but they
were all slow and not particularly powerful. However for the past few
years, I've enjoyed
[DraftSight](http://www.3ds.com/products-services/draftsight/overview/).

[<img src="http://a1.media.3ds.com/fileadmin/PRODUCTS/DRAFT_SIGHT/IMG/400x267_Product_Screen.jpg" alt="DraftSight - a free and cross-platform alternative to AutoCAD" class="alignnone" width="400" height="267" />](http://www.3ds.com/products-services/draftsight/overview/)

Firstly, a disclaimer: DraftSight is *not* open-source. It is certainly
free to use and works very well on all platforms, but it is backed by a
commercial company (Dassault Systemes), is financed through an
enterprise license, and certainly has no obligation to the community.

However perhaps the reason DraftSight is so much more powerful than the
open-source alternatives is because it has a very clear goal: it wants
to clone AutoCAD. Unlike GIMP, which has tried to define a new paradigm
for itself, DraftSight keeps users in a familiar environment.

If you are on Gentoo Linux, I am maintaining the DraftSight package and
as of May earlier this year, it is available in the betagarden overlay.
For more information, you can see [this
bug](https://bugs.gentoo.org/show_bug.cgi?id=359805).
