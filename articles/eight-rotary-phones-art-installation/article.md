# Eight rotary phones - an art installation

Late last year, I helped run the University of Sydney annual graduation
exhibition for the Architecture faculty. One of the things I was
responsible for was helping set up an "artistic" installation.
Architects have strange concepts of what is and isn't art, and
apparently an isolated network of eight rotary phones qualifies.

![An old rotary phone](rotary-phone.jpg)

The concept was simple: throw eight hipster phones around with a few
numbers and see what happens. You could call each other around the
building. I'm sorry, were you expecting more? Nope. That's art for you.

It did, however, give me an opportunity to learn the basics of
traditional phone systems - from things like pulses, tones, VOIPs, PABX,
switchboards, right down to the physical labour of installing more than
200 meters of phone cable across a building.

On the night itself, I'm happy to say that the installation (in both the
technical and artistic sense) was a success. I've never heard such
creative instant role playing or even inappropriate words said to
would-be employers.

... I wonder how long I can keep that phone network running before
people realise it's not a legitimate part of their system?
