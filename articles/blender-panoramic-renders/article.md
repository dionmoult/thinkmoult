# Blender panoramic renders howto and demonstration

Doing a Blender panoramic render isn't easy. This is because you can't
actually see what you're going to create in the regular OpenGL-shaded 3D
view. In fact, even when looking through the Blender camera, you won't
see what you're going to end up with.

## Blender panoramic renders with Blender Internal renderer

The technique is actually rather simple: just select the camera. Then in
the object data settings panel, just click the panoramic button and play
with the focal length until you get something you want. You can see an
example of how to create a simple Blender panoramic render in the
screenshot below:

![Blender panoramic settings](blender_panoramic_settings.png)

You'll soon discover is that you can only actually see what you're going
to get when you hit the render button. If you're using the internal
Blender renderer, this'll become a pain really quickly. This is because
you'll have to constantly hit render and wait for the image to come up.

## Blender panoramic renders with Cycles renderer

However if you switch the scene to Cycles Renderer, things become
easier. This is because Cycles provides a real-time rendering view. Just
set one window open to rendered view and you'll see what you're going to
get (roughly) in real time. At the same time, you'll notice that Cycles
opens up new options on how your panoramic render should be like: the
type of panoramic - whether it's fish eye, equidistant, or rectangular,
and how many degrees the field of view is.

![Blender cycles panoramic camera settings](blender_cycles_panoramic.png)

Now comes the fun part, actually building your scene. The best strategy
is to have planned out your scene beforehand on paper so you know its
composition. With panoramic renders, if you haven't already got a scene
built, now is your chance to bend reality in a Dali-esque manner and
have fun with perspective.

Once your composition is planned out, create simple cubes and planes as
proxy objects for the rough locations of where your scene objects are
going to be. Then slowly replace them with the more detailed meshes of
what you want to make.

Another useful tip is to use the Blender preview window. In Cycles, when
in camera view, just press shift-b and drag and drop a cropped portion
of your screen, and it'll only render that area. This keeps rendering
times low. There are plenty of other tips on how to [reduce rendering
times](http://www.blenderguru.com/13-ways-to-reduce-render-times/), so I
won't repeat them all here.

Below you can see a panoramic render I did a few months ago which uses
the above techniques. As you can see, I've blended several scenes
together with varying perspectives, which was only possible with a
panoramic render.

![Blender panoramic render](blender_panoramic_render.png)

Good luck, and create those awesome renders!
