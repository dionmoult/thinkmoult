# Browse Amarok's embedded MySQL database

Amarok can probably be called one of Linux's flagship programs. However
since the upgrade from Amarok 1 to Amarok 2 there have been quite a
number of controversial changes. One of these changes is that Amarok
switched from an SQLite database to a MySQL database to store song
information, however whether or not this was the right move is not the
topic of this post.

With this new database, users were given two choices - an external
database or an embedded database. The embedded database was created to
simplify the setup for users who weren't comfortable with the idea of
manually setting up a MySQL database. But what happens if you have an
embedded database and afterwards you _do_ want to mess around with it
and look inside? For whatever purpose be it bugfixing, locating a
specific bit of information, or bulk song management (nothing beats a
good query!), sometimes you'd want to do this.

Amarok stores its embedded database information in
`$KDEHOME/share/apps/amarok/mysqle/amarok/` - where `$KDEHOME` is
usually `~/.kde`. So as long as you have MySQL setup elsewhere, all you
have to do is create a blank database, and dump all of these files where
MySQL stores its information. This location is MySQL's *datadir*, which
is set inside the `my.cnf` configuration file, normally placed in
`/etc/mysql/my.cnf`. In a regular install, your data dir will be in
`/var/lib/mysql/` - and will contain one directory per database. So just
copy over Amarok's database files into the database's directory. The
final step is to ensure the files are owned by the mysql user, done by
`chown mysql:mysql`.

Now you can browse the database normally through your preferred method
(command line, PHPMyAdmin, or other MySQL client)

That's it! I hope this is useful to somebody.
