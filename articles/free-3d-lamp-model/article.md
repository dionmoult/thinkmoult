# Free 3D lamp model

I modeled this hinged, office desk / table lamp. It's suitable for task lighting
and has a cable extending out. Units are in meters, and the file is available for
download in Blender and OBJ format.

Very basic materials are provided in the form of Cycles shaders.

 - [Download .obj](3d-lamp.zip) (3.6M)
 - [Download .blend](3d-lamp.blend) (490K)

## Images

![An example render of the 3D lamp model](3d-lamp-overall.png)

![Zoom up render of the luminaire head](3d-lamp-head.png)

![Zoom up render of the lamp base](3d-lamp-bottom.png)
