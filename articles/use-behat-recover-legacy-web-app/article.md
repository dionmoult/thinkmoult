# Use Behat to recover your legacy web app

Your legacy project is going to the dogs. No problem, with this easy 7
line code patch you can now fix your entire codebase within a weekend!

    /**
     * @Given /^I will write this test in the future$/
     */
    public function iWillWriteThisTestInTheFuture()
    {
        throw new PendingException();
    }

Go you! :)
