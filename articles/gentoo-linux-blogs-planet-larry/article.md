# Gentoo Linux blogs on Planet Larry

If you use [Gentoo Linux](http://gentoo.org/), you probably know that
you find Gentoo Linux blogs on [Planet
Gentoo](https://planet.gentoo.org/). If you haven't heard of a planet
before, A [planet](https://en.wikipedia.org/wiki/Planet_(software)) is a
website that aggregates a series of blog feeds, and most open-source
communities have one. For example, there is also [Planet
KDE](https://planet.kde.org/) and [Planet
GNOME](http://planet.gnome.org/). Planet Gentoo, however, is limited to
the topic of Gentoo Linux itself, and only aggregates content by Gentoo
developers. In the past, Steve Dibb (beandog) started up planet Larry,
named after the Gentoo mascot
"[Larry](https://wiki.gentoo.org/wiki/Project:Artwork/Artwork)", which
hosted blogs of Gentoo users. Naturally, Gentoo users get up to all sort
of interesting endeavours, and so begun a slightly less technical, less
stringently project-specific blog feed. Here's a picture of Larry below.

[![Larry the cow
mascot](Larry-the-cow-full.png)](http://planetlarry.org/)

Unfortunately, recently after checking back at my old feedreader list, I
noticed that [Planet Larry had gone
AWOL](https://forums.gentoo.org/viewtopic-t-1074872.html), and so
decided to recreate it. It was never an official Gentoo project and
Steve Dibb didn't seem around, and the domain name (larrythecow.org) at
the time seemed to be squatted on by advertisers. If you visit it now,
despite a half-baked attempt at a Gentoo-ish theme it was filled with
"laptop recommendations". Instead, I registered
[planetlarry.org](http://planetlarry.org/) and started up a new
aggregator. The concept is the same as the original. In short:

-   If you use Gentoo Linux and write a blog which has a feed, you can
    add your blog to Planet Larry
-   You can write about anything you want, as often as you want. It
    doesn't necessarily need to be related to Gentoo Linux at all --
    although I did find that most Gentoo Linux Blogs seem to have more
    technical content.

So, go ahead and check out [PlanetLarry.org](http://planetlarry.org/).
If you contact me I will add your blog.

Credits for the Larry the cow male graphic go to Ethan Dunham and Matteo
Pescarin, licensed under CC-BY-SA/2.5.
