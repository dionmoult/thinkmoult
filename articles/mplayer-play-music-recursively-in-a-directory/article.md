# How to use MPlayer to play music recursively in a directory

I have always wondered how to do this. It's quite often I have
directories full of media files (specifically music) and subdirectories
within them also with music files, and though the manual for MPlayer is
thicker than the Chinese phonebook I have not been able to find any
option for it. I did however find a method which isn't exactly the guru
one-liner, but here it is anyway. It's broken into two steps, the first
to create a playlist, and then to play the playlist.

```
$ find -maxdepth 1 -type f -name \*.\* > playlist
$ mplayer -playlist playlist
```

Just add a `-loop 0` suffix if you want to loop :)
