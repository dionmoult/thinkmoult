# A proposed Radiance Filesystem Hierarchy Standard

Radiance projects do not usually have any form of enforced structure for organising your files. This is convenient, as Radiance is a whole suite of lighting analysis tools that can be tied together in any way. It is inappropriate to enforce one, as it would be akin to asking `imagemagick` to require your raster files be in a certain folder structure. However, this creates an issue when a scene grows to be rather complex, as Radiance scenes have many files. It also means that it can be hard to distribute or reuse assets across projects and across organisations, as everybody has their own way or organising files.

To allow the easy maintenance of a complex scene and the reuse of assets, I would like to propose a Radiance Filesystem Hierarchy Standard, or RHFS. It is not strict, but I have found it useful in my works, and will use it to distribute Radiance assets online. If you are planning on using it to build something, please let me know!

## Structure of a single asset

The following rules describe how to create a single, reusable asset. This may be an object, such as a chair, table, or lamp.

 1. A single asset must be fully contained in a folder named after the asset. This folder should be distributed as an atomic package and have no other dependencies.
 2. The asset name must be all lowercase, and spaces should be represented by hyphens. This improves SEO and can be used in programming contexts.
 3. The asset must only contain the raw source in open formats. All other data that can be generated should be excluded. This minimises repository filesize.
 4. If data needs to be generated, the asset must contain a `Makefile` at its top level directory, providing a `make lib` target at a minimum to build all necessary data.
 5. The asset must have a `README.md` at its top level directory, describing the asset, and explaining how to use it in a project
 7. The asset must have a `LICENSE` at its top level directory, so that the license doesn't get lost with asset transfer
 8. The asset may have zero or more of the following directories, to hold its data
    - `obj/` - stores all "objects", such as `.rad`, `.rtm`, `.obj`, `.oct`, and parametric source data such as `.blend`, `.rfa`, or `.3dm`
    - `tex/` - stores all "textures" (in the CG sense), such as `.hdr`, and source data such as `.xcf`, `.svg`, or `.psd`
    - `cal/` - stores all calculation files
    - `ies/` - stores all light files
    - `mat/` - stores all material definitions, such as `.mat` files
    - `pic/` - stores all example pictures

This is an example of the asset `cool-chair`.

```
cool-chair/
cool-chair/obj/
cool-chair/obj/cool-chair.blend # the source file, if anybody wants to tweak
cool-chair/obj/cool-chair.obj # the baked geometry
cool-chair/obj/cool-chair.rad # radiance mesh primitive, in this case
cool-chair/tex/
cool-chair/tex/leather.hdr # an example texture
cool-chair/tex/leather.xcf # the GIMP file used to paint the leather texture
cool-chair/cal/
cool-chair/cal/led-colour-customiser.cal # this leather chair has funky LED effects!
cool-chair/ies/
cool-chair/ies/led-strip.ies # an example ies file
cool-chair/mat/
cool-chair/mat/cool-chair.mat # material file
cool-chair/Makefile
cool-chair/README.md
cool-chair/LICENSE
```

## Including an asset in your Radiance scene

The following rules describe how to use the asset in a scene that you are building. These are required as Radiance paths are relative to where the scene is compiled and rendered.

 1. The project must have a `lib/` directory at its top level directory, which will be the working directory where the scene will be compiled and rendered.
 2. Each asset directory must be located in the `lib/` directory.

In addition, to make it easier for others to view your scene, it is recommended that the following rules be followed when creating a Radiance scene:

 1. The project should have a `README.md` at its top level directory, describing the scene
 2. The project should have a `LICENSE` at its top level directory
 3. The project should be a `pic/` directory which stores renders created with the scene
 4. The project should have a `Makefile` at its top level directory, providing the following targets:
    - `make libs` - build all asset libs
    - `make view` - interactively view the scene
    - `make render` - render the scene

When using an asset in your scene, the asset's `README.md` file should explain how to include the asset in the scene. This could be anything, as Radiance can be quite complex. However, it is recommended that the asset provides a `.rad` or an `.oct` file. This is so that it can be simply included with `xform` or instanced in with a single line in the project. For example, a project can use our `cool-chair` asset by simply typing in one line:

```
!xform lib/cool-chair/obj/cool-chair.rad
```

This is an example project which follows the rules above:

```
lib/
lib/cool-chair/*
pic/
pic/scene.hdr # some milestone render
scene.rad # the above xform command lives here
scene.rif
Makefile
README.md
LICENSE
```

Here is an example `Makefile` that provides the expected targets. The details may be different, but if these targets exist as a convention it may make projects a little bit easier to run.

```
.PHONY : libs
libs :
    cd lib/cool-chair && make lib

.PHONY : view
view :
    make libs
    rad -o x11 scene.rif

.PHONY : render
render :
    make libs
    rad scene.rif
```

## Other recommendations

 1. It should be assumed that all assets can be further customised. Provide source geometry in formats that FOSS software can open, such as `.blend`, `.xcf`, or `.svg`.
 2. If your asset has multiple variations, allow them to be built. For example, if `cool-chair` comes in different materials, describe its usage in `README.md` and provide convenience targets in `Makefile` to build things differently based on different materials in its `mat/` directory.
 3. Track the asset in a `git` repository and use `git lfs` for tracking large binary assets, such as `*.obj`, `*.blend` and `*.hdr`
