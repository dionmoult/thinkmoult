# Architectural visualisation renders with Blender

It's been a while since I've made a post. Although there are posts in
the queue, I figured I might post this as it's a quick one. Let's see
the pictures first.

[![Visualisation 1](untitled_postpro22.png)](untitled_postpro22.png)

... and the other ...

[![Visualisation 2](ageawef.png)](ageawef.png)

Images done with Blender and Cycles. Piano in second render done by
RegusTtef. These images are 50% of actual size, together these images
took a day and a half. Second image is a panoramic shot.

The building is a proposal for the Sydney Museum of Profligate Steel
Welders. The rest writes itself :)
