# A short and simple beginners look at Markdown

At [SevenStrokes](http://sevenstrokes.net), we forego email support and
go straight to a forum / discussion-based system based off
[Vanilla](http://vanillaforums.org). This is great, because we can
organise client discussions much better, focus discussions on certain
topics, split and merge topics as they spin off from original topics,
and through an intuitive interface that takes no time to learn. Best of
all, we can escape from those badly formatted client emails with the
annoying 10-line signature and get to the point. That's the reason our
discussion post formatting is based off *Markdown*.

Too bad it's not obvious enough how to use Markdown.

I wrote this very short, basic, and purposely omitting details guide to
[What is Markdown?](http://sevenstrokes.net/what-is-markdown) - I hope
you like it :)
